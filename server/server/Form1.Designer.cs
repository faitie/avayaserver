﻿namespace server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnServerStart = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCloseConnections = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsServerIp = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsServerPort = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsServerPID = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpStartpage = new System.Windows.Forms.TabPage();
            this.lbData = new System.Windows.Forms.ListBox();
            this.tpLogging = new System.Windows.Forms.TabPage();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDuurtot = new System.Windows.Forms.DateTimePicker();
            this.dtpDuurvan = new System.Windows.Forms.DateTimePicker();
            this.tbSearchExtension = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGridRefresh = new System.Windows.Forms.Button();
            this.gridLogging = new System.Windows.Forms.DataGridView();
            this.tpAdmin = new System.Windows.Forms.TabPage();
            this.splitContainerAdmin = new System.Windows.Forms.SplitContainer();
            this.splitContainerCommands = new System.Windows.Forms.SplitContainer();
            this.splitContainerDetail = new System.Windows.Forms.SplitContainer();
            this.lbSubEvents = new System.Windows.Forms.ListBox();
            this.lbUncomMsg = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSubEvents = new System.Windows.Forms.Label();
            this.txtNrOfCalls = new System.Windows.Forms.TextBox();
            this.lblNrOfCalls = new System.Windows.Forms.Label();
            this.txtTimeOfFirstLogin = new System.Windows.Forms.TextBox();
            this.lblFirstLoginTime = new System.Windows.Forms.Label();
            this.lblOffice = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lblPersId = new System.Windows.Forms.Label();
            this.txtOffice = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtPersId = new System.Windows.Forms.TextBox();
            this.txtExtension = new System.Windows.Forms.TextBox();
            this.lblExtension = new System.Windows.Forms.Label();
            this.txtSeqNr = new System.Windows.Forms.TextBox();
            this.txtPortReceivingR = new System.Windows.Forms.TextBox();
            this.txtPortReceivingL = new System.Windows.Forms.TextBox();
            this.txtPortSending = new System.Windows.Forms.TextBox();
            this.txtPID = new System.Windows.Forms.TextBox();
            this.lblSeqNr = new System.Windows.Forms.Label();
            this.lblPortRecR = new System.Windows.Forms.Label();
            this.lblPortRecL = new System.Windows.Forms.Label();
            this.lblPortSend = new System.Windows.Forms.Label();
            this.lblProcessID = new System.Windows.Forms.Label();
            this.txtIPNumber = new System.Windows.Forms.TextBox();
            this.lblIP = new System.Windows.Forms.Label();
            this.btnAdminExec = new System.Windows.Forms.Button();
            this.txtAdminCommands = new System.Windows.Forms.TextBox();
            this.cbxAdminCommands = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.logMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tpStartpage.SuspendLayout();
            this.tpLogging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLogging)).BeginInit();
            this.tpAdmin.SuspendLayout();
            this.splitContainerAdmin.Panel2.SuspendLayout();
            this.splitContainerAdmin.SuspendLayout();
            this.splitContainerCommands.Panel1.SuspendLayout();
            this.splitContainerCommands.Panel2.SuspendLayout();
            this.splitContainerCommands.SuspendLayout();
            this.splitContainerDetail.Panel1.SuspendLayout();
            this.splitContainerDetail.Panel2.SuspendLayout();
            this.splitContainerDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnServerStart
            // 
            this.btnServerStart.Location = new System.Drawing.Point(6, 6);
            this.btnServerStart.Name = "btnServerStart";
            this.btnServerStart.Size = new System.Drawing.Size(72, 50);
            this.btnServerStart.TabIndex = 1;
            this.btnServerStart.Text = "SERVER START";
            this.btnServerStart.UseVisualStyleBackColor = true;
            this.btnServerStart.Click += new System.EventHandler(this.btnServerStart_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1027, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(68, 50);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "SHOW OBJECTS";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCloseConnections
            // 
            this.btnCloseConnections.Location = new System.Drawing.Point(84, 6);
            this.btnCloseConnections.Name = "btnCloseConnections";
            this.btnCloseConnections.Size = new System.Drawing.Size(100, 50);
            this.btnCloseConnections.TabIndex = 3;
            this.btnCloseConnections.Text = "CLOSE ALL CONNECTIONS";
            this.btnCloseConnections.UseVisualStyleBackColor = true;
            this.btnCloseConnections.Click += new System.EventHandler(this.btnCloseConnections_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsServerIp,
            this.tsServerPort,
            this.tsServerPID});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1111, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsServerIp
            // 
            this.tsServerIp.Name = "tsServerIp";
            this.tsServerIp.Size = new System.Drawing.Size(0, 17);
            // 
            // tsServerPort
            // 
            this.tsServerPort.Name = "tsServerPort";
            this.tsServerPort.Size = new System.Drawing.Size(0, 17);
            // 
            // tsServerPID
            // 
            this.tsServerPID.Name = "tsServerPID";
            this.tsServerPID.Size = new System.Drawing.Size(0, 17);
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpStartpage);
            this.tcMain.Controls.Add(this.tpLogging);
            this.tcMain.Controls.Add(this.tpAdmin);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcMain.Location = new System.Drawing.Point(0, 22);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1111, 547);
            this.tcMain.TabIndex = 5;
            // 
            // tpStartpage
            // 
            this.tpStartpage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tpStartpage.Controls.Add(this.lbData);
            this.tpStartpage.Controls.Add(this.btnServerStart);
            this.tpStartpage.Controls.Add(this.btnCloseConnections);
            this.tpStartpage.Controls.Add(this.btnPrint);
            this.tpStartpage.ForeColor = System.Drawing.Color.Black;
            this.tpStartpage.Location = new System.Drawing.Point(4, 24);
            this.tpStartpage.Name = "tpStartpage";
            this.tpStartpage.Padding = new System.Windows.Forms.Padding(3);
            this.tpStartpage.Size = new System.Drawing.Size(1103, 519);
            this.tpStartpage.TabIndex = 0;
            this.tpStartpage.Text = "startpage";
            this.tpStartpage.UseVisualStyleBackColor = true;
            // 
            // lbData
            // 
            this.lbData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbData.FormattingEnabled = true;
            this.lbData.ItemHeight = 15;
            this.lbData.Location = new System.Drawing.Point(3, 107);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(1097, 409);
            this.lbData.TabIndex = 4;
            // 
            // tpLogging
            // 
            this.tpLogging.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tpLogging.Controls.Add(this.btnSearch);
            this.tpLogging.Controls.Add(this.label3);
            this.tpLogging.Controls.Add(this.label2);
            this.tpLogging.Controls.Add(this.dtpDuurtot);
            this.tpLogging.Controls.Add(this.dtpDuurvan);
            this.tpLogging.Controls.Add(this.tbSearchExtension);
            this.tpLogging.Controls.Add(this.label1);
            this.tpLogging.Controls.Add(this.btnGridRefresh);
            this.tpLogging.Controls.Add(this.gridLogging);
            this.tpLogging.ForeColor = System.Drawing.Color.Black;
            this.tpLogging.Location = new System.Drawing.Point(4, 24);
            this.tpLogging.Name = "tpLogging";
            this.tpLogging.Padding = new System.Windows.Forms.Padding(3);
            this.tpLogging.Size = new System.Drawing.Size(1103, 519);
            this.tpLogging.TabIndex = 1;
            this.tpLogging.Text = "logging";
            this.tpLogging.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(501, 17);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(59, 49);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Zoeken";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "DUURTOT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "DUURVAN";
            // 
            // dtpDuurtot
            // 
            this.dtpDuurtot.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDuurtot.Location = new System.Drawing.Point(281, 51);
            this.dtpDuurtot.Name = "dtpDuurtot";
            this.dtpDuurtot.Size = new System.Drawing.Size(200, 21);
            this.dtpDuurtot.TabIndex = 5;
            this.dtpDuurtot.Value = new System.DateTime(2013, 3, 28, 23, 59, 0, 0);
            // 
            // dtpDuurvan
            // 
            this.dtpDuurvan.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDuurvan.Location = new System.Drawing.Point(281, 16);
            this.dtpDuurvan.Name = "dtpDuurvan";
            this.dtpDuurvan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpDuurvan.Size = new System.Drawing.Size(200, 21);
            this.dtpDuurvan.TabIndex = 4;
            this.dtpDuurvan.Value = new System.DateTime(2013, 3, 28, 0, 1, 0, 0);
            // 
            // tbSearchExtension
            // 
            this.tbSearchExtension.Location = new System.Drawing.Point(44, 37);
            this.tbSearchExtension.Name = "tbSearchExtension";
            this.tbSearchExtension.Size = new System.Drawing.Size(118, 21);
            this.tbSearchExtension.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "EXTENSION";
            // 
            // btnGridRefresh
            // 
            this.btnGridRefresh.Location = new System.Drawing.Point(592, 17);
            this.btnGridRefresh.Name = "btnGridRefresh";
            this.btnGridRefresh.Size = new System.Drawing.Size(59, 49);
            this.btnGridRefresh.TabIndex = 1;
            this.btnGridRefresh.Text = "Refresh";
            this.btnGridRefresh.UseVisualStyleBackColor = true;
            this.btnGridRefresh.Click += new System.EventHandler(this.button1_Click);
            // 
            // gridLogging
            // 
            this.gridLogging.AllowUserToAddRows = false;
            this.gridLogging.AllowUserToDeleteRows = false;
            this.gridLogging.BackgroundColor = System.Drawing.Color.White;
            this.gridLogging.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(88)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridLogging.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridLogging.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridLogging.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gridLogging.Location = new System.Drawing.Point(3, 87);
            this.gridLogging.Name = "gridLogging";
            this.gridLogging.ReadOnly = true;
            this.gridLogging.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridLogging.Size = new System.Drawing.Size(1097, 429);
            this.gridLogging.TabIndex = 0;
            // 
            // tpAdmin
            // 
            this.tpAdmin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.tpAdmin.Controls.Add(this.splitContainerAdmin);
            this.tpAdmin.ForeColor = System.Drawing.Color.Black;
            this.tpAdmin.Location = new System.Drawing.Point(4, 24);
            this.tpAdmin.Name = "tpAdmin";
            this.tpAdmin.Size = new System.Drawing.Size(1103, 519);
            this.tpAdmin.TabIndex = 2;
            this.tpAdmin.Text = "admin";
            this.tpAdmin.UseVisualStyleBackColor = true;
            // 
            // splitContainerAdmin
            // 
            this.splitContainerAdmin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAdmin.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAdmin.Name = "splitContainerAdmin";
            // 
            // splitContainerAdmin.Panel1
            // 
            this.splitContainerAdmin.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            // 
            // splitContainerAdmin.Panel2
            // 
            this.splitContainerAdmin.Panel2.Controls.Add(this.splitContainerCommands);
            this.splitContainerAdmin.Size = new System.Drawing.Size(1103, 519);
            this.splitContainerAdmin.SplitterDistance = 301;
            this.splitContainerAdmin.TabIndex = 0;
            // 
            // splitContainerCommands
            // 
            this.splitContainerCommands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerCommands.Location = new System.Drawing.Point(0, 0);
            this.splitContainerCommands.Name = "splitContainerCommands";
            // 
            // splitContainerCommands.Panel1
            // 
            this.splitContainerCommands.Panel1.Controls.Add(this.splitContainerDetail);
            // 
            // splitContainerCommands.Panel2
            // 
            this.splitContainerCommands.Panel2.Controls.Add(this.btnAdminExec);
            this.splitContainerCommands.Panel2.Controls.Add(this.txtAdminCommands);
            this.splitContainerCommands.Panel2.Controls.Add(this.cbxAdminCommands);
            this.splitContainerCommands.Panel2.Controls.Add(this.label5);
            this.splitContainerCommands.Size = new System.Drawing.Size(796, 517);
            this.splitContainerCommands.SplitterDistance = 476;
            this.splitContainerCommands.TabIndex = 0;
            // 
            // splitContainerDetail
            // 
            this.splitContainerDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerDetail.Location = new System.Drawing.Point(0, 0);
            this.splitContainerDetail.Name = "splitContainerDetail";
            this.splitContainerDetail.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerDetail.Panel1
            // 
            this.splitContainerDetail.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.splitContainerDetail.Panel1.Controls.Add(this.lbSubEvents);
            this.splitContainerDetail.Panel1.Controls.Add(this.lbUncomMsg);
            this.splitContainerDetail.Panel1.Controls.Add(this.label4);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblSubEvents);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtNrOfCalls);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblNrOfCalls);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtTimeOfFirstLogin);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblFirstLoginTime);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblOffice);
            this.splitContainerDetail.Panel1.Controls.Add(this.lbName);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblPersId);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtOffice);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtName);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtPersId);
            this.splitContainerDetail.Panel1.Controls.Add(this.txtExtension);
            this.splitContainerDetail.Panel1.Controls.Add(this.lblExtension);
            // 
            // splitContainerDetail.Panel2
            // 
            this.splitContainerDetail.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.splitContainerDetail.Panel2.Controls.Add(this.txtSeqNr);
            this.splitContainerDetail.Panel2.Controls.Add(this.txtPortReceivingR);
            this.splitContainerDetail.Panel2.Controls.Add(this.txtPortReceivingL);
            this.splitContainerDetail.Panel2.Controls.Add(this.txtPortSending);
            this.splitContainerDetail.Panel2.Controls.Add(this.txtPID);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblSeqNr);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblPortRecR);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblPortRecL);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblPortSend);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblProcessID);
            this.splitContainerDetail.Panel2.Controls.Add(this.txtIPNumber);
            this.splitContainerDetail.Panel2.Controls.Add(this.lblIP);
            this.splitContainerDetail.Size = new System.Drawing.Size(476, 517);
            this.splitContainerDetail.SplitterDistance = 320;
            this.splitContainerDetail.TabIndex = 1;
            // 
            // lbSubEvents
            // 
            this.lbSubEvents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbSubEvents.FormattingEnabled = true;
            this.lbSubEvents.ItemHeight = 15;
            this.lbSubEvents.Location = new System.Drawing.Point(21, 182);
            this.lbSubEvents.MinimumSize = new System.Drawing.Size(206, 109);
            this.lbSubEvents.Name = "lbSubEvents";
            this.lbSubEvents.Size = new System.Drawing.Size(206, 109);
            this.lbSubEvents.TabIndex = 21;
            // 
            // lbUncomMsg
            // 
            this.lbUncomMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbUncomMsg.FormattingEnabled = true;
            this.lbUncomMsg.ItemHeight = 15;
            this.lbUncomMsg.Location = new System.Drawing.Point(247, 182);
            this.lbUncomMsg.MinimumSize = new System.Drawing.Size(206, 109);
            this.lbUncomMsg.Name = "lbUncomMsg";
            this.lbUncomMsg.Size = new System.Drawing.Size(206, 109);
            this.lbUncomMsg.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(336, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Uncommited Msg";
            // 
            // lblSubEvents
            // 
            this.lblSubEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSubEvents.AutoSize = true;
            this.lblSubEvents.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubEvents.Location = new System.Drawing.Point(18, 162);
            this.lblSubEvents.Name = "lblSubEvents";
            this.lblSubEvents.Size = new System.Drawing.Size(131, 17);
            this.lblSubEvents.TabIndex = 18;
            this.lblSubEvents.Text = "Subscribed Events";
            // 
            // txtNrOfCalls
            // 
            this.txtNrOfCalls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNrOfCalls.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNrOfCalls.Location = new System.Drawing.Point(152, 124);
            this.txtNrOfCalls.Name = "txtNrOfCalls";
            this.txtNrOfCalls.Size = new System.Drawing.Size(306, 21);
            this.txtNrOfCalls.TabIndex = 12;
            // 
            // lblNrOfCalls
            // 
            this.lblNrOfCalls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNrOfCalls.AutoSize = true;
            this.lblNrOfCalls.Font = new System.Drawing.Font("Arial", 11.25F);
            this.lblNrOfCalls.Location = new System.Drawing.Point(18, 125);
            this.lblNrOfCalls.Name = "lblNrOfCalls";
            this.lblNrOfCalls.Size = new System.Drawing.Size(113, 17);
            this.lblNrOfCalls.TabIndex = 11;
            this.lblNrOfCalls.Text = "Number of Calls";
            // 
            // txtTimeOfFirstLogin
            // 
            this.txtTimeOfFirstLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimeOfFirstLogin.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTimeOfFirstLogin.Location = new System.Drawing.Point(152, 95);
            this.txtTimeOfFirstLogin.Name = "txtTimeOfFirstLogin";
            this.txtTimeOfFirstLogin.Size = new System.Drawing.Size(306, 21);
            this.txtTimeOfFirstLogin.TabIndex = 10;
            // 
            // lblFirstLoginTime
            // 
            this.lblFirstLoginTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFirstLoginTime.AutoSize = true;
            this.lblFirstLoginTime.Font = new System.Drawing.Font("Arial", 11.25F);
            this.lblFirstLoginTime.Location = new System.Drawing.Point(18, 96);
            this.lblFirstLoginTime.Name = "lblFirstLoginTime";
            this.lblFirstLoginTime.Size = new System.Drawing.Size(128, 17);
            this.lblFirstLoginTime.TabIndex = 9;
            this.lblFirstLoginTime.Text = "Time of First Login";
            // 
            // lblOffice
            // 
            this.lblOffice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOffice.AutoSize = true;
            this.lblOffice.Font = new System.Drawing.Font("Arial", 11.25F);
            this.lblOffice.Location = new System.Drawing.Point(212, 43);
            this.lblOffice.Name = "lblOffice";
            this.lblOffice.Size = new System.Drawing.Size(47, 17);
            this.lblOffice.TabIndex = 8;
            this.lblOffice.Text = "Office";
            // 
            // lbName
            // 
            this.lbName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Arial", 11.25F);
            this.lbName.Location = new System.Drawing.Point(77, 43);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(47, 17);
            this.lbName.TabIndex = 7;
            this.lbName.Text = "Name";
            // 
            // lblPersId
            // 
            this.lblPersId.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPersId.AutoSize = true;
            this.lblPersId.Font = new System.Drawing.Font("Arial", 11.25F);
            this.lblPersId.Location = new System.Drawing.Point(18, 43);
            this.lblPersId.Name = "lblPersId";
            this.lblPersId.Size = new System.Drawing.Size(51, 17);
            this.lblPersId.TabIndex = 6;
            this.lblPersId.Text = "persID";
            // 
            // txtOffice
            // 
            this.txtOffice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOffice.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOffice.Location = new System.Drawing.Point(215, 63);
            this.txtOffice.Name = "txtOffice";
            this.txtOffice.Size = new System.Drawing.Size(243, 21);
            this.txtOffice.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(80, 63);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(129, 21);
            this.txtName.TabIndex = 4;
            // 
            // txtPersId
            // 
            this.txtPersId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtPersId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersId.Location = new System.Drawing.Point(21, 63);
            this.txtPersId.Name = "txtPersId";
            this.txtPersId.Size = new System.Drawing.Size(50, 21);
            this.txtPersId.TabIndex = 3;
            // 
            // txtExtension
            // 
            this.txtExtension.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtension.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExtension.Location = new System.Drawing.Point(96, 16);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(362, 21);
            this.txtExtension.TabIndex = 1;
            // 
            // lblExtension
            // 
            this.lblExtension.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExtension.AutoSize = true;
            this.lblExtension.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtension.Location = new System.Drawing.Point(18, 17);
            this.lblExtension.Name = "lblExtension";
            this.lblExtension.Size = new System.Drawing.Size(72, 17);
            this.lblExtension.TabIndex = 0;
            this.lblExtension.Text = "Extension";
            // 
            // txtSeqNr
            // 
            this.txtSeqNr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSeqNr.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeqNr.Location = new System.Drawing.Point(139, 153);
            this.txtSeqNr.Name = "txtSeqNr";
            this.txtSeqNr.Size = new System.Drawing.Size(319, 21);
            this.txtSeqNr.TabIndex = 12;
            // 
            // txtPortReceivingR
            // 
            this.txtPortReceivingR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPortReceivingR.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPortReceivingR.Location = new System.Drawing.Point(139, 126);
            this.txtPortReceivingR.Name = "txtPortReceivingR";
            this.txtPortReceivingR.Size = new System.Drawing.Size(319, 21);
            this.txtPortReceivingR.TabIndex = 11;
            // 
            // txtPortReceivingL
            // 
            this.txtPortReceivingL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPortReceivingL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPortReceivingL.Location = new System.Drawing.Point(139, 99);
            this.txtPortReceivingL.Name = "txtPortReceivingL";
            this.txtPortReceivingL.Size = new System.Drawing.Size(319, 21);
            this.txtPortReceivingL.TabIndex = 10;
            // 
            // txtPortSending
            // 
            this.txtPortSending.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPortSending.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPortSending.Location = new System.Drawing.Point(139, 72);
            this.txtPortSending.Name = "txtPortSending";
            this.txtPortSending.Size = new System.Drawing.Size(319, 21);
            this.txtPortSending.TabIndex = 9;
            // 
            // txtPID
            // 
            this.txtPID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPID.Location = new System.Drawing.Point(139, 45);
            this.txtPID.Name = "txtPID";
            this.txtPID.Size = new System.Drawing.Size(319, 21);
            this.txtPID.TabIndex = 8;
            // 
            // lblSeqNr
            // 
            this.lblSeqNr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSeqNr.AutoSize = true;
            this.lblSeqNr.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeqNr.Location = new System.Drawing.Point(18, 154);
            this.lblSeqNr.Name = "lblSeqNr";
            this.lblSeqNr.Size = new System.Drawing.Size(93, 17);
            this.lblSeqNr.TabIndex = 7;
            this.lblSeqNr.Text = "Sequence Nr";
            // 
            // lblPortRecR
            // 
            this.lblPortRecR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortRecR.AutoSize = true;
            this.lblPortRecR.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPortRecR.Location = new System.Drawing.Point(18, 127);
            this.lblPortRecR.Name = "lblPortRecR";
            this.lblPortRecR.Size = new System.Drawing.Size(118, 17);
            this.lblPortRecR.TabIndex = 6;
            this.lblPortRecR.Text = "Port Receiving R";
            // 
            // lblPortRecL
            // 
            this.lblPortRecL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortRecL.AutoSize = true;
            this.lblPortRecL.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPortRecL.Location = new System.Drawing.Point(18, 100);
            this.lblPortRecL.Name = "lblPortRecL";
            this.lblPortRecL.Size = new System.Drawing.Size(115, 17);
            this.lblPortRecL.TabIndex = 5;
            this.lblPortRecL.Text = "Port Receiving L";
            // 
            // lblPortSend
            // 
            this.lblPortSend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortSend.AutoSize = true;
            this.lblPortSend.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPortSend.Location = new System.Drawing.Point(18, 73);
            this.lblPortSend.Name = "lblPortSend";
            this.lblPortSend.Size = new System.Drawing.Size(92, 17);
            this.lblPortSend.TabIndex = 4;
            this.lblPortSend.Text = "Port Sending";
            // 
            // lblProcessID
            // 
            this.lblProcessID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProcessID.AutoSize = true;
            this.lblProcessID.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessID.Location = new System.Drawing.Point(18, 46);
            this.lblProcessID.Name = "lblProcessID";
            this.lblProcessID.Size = new System.Drawing.Size(81, 17);
            this.lblProcessID.TabIndex = 3;
            this.lblProcessID.Text = "Process ID";
            // 
            // txtIPNumber
            // 
            this.txtIPNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIPNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIPNumber.Location = new System.Drawing.Point(139, 18);
            this.txtIPNumber.Name = "txtIPNumber";
            this.txtIPNumber.Size = new System.Drawing.Size(319, 21);
            this.txtIPNumber.TabIndex = 2;
            // 
            // lblIP
            // 
            this.lblIP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIP.AutoSize = true;
            this.lblIP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIP.Location = new System.Drawing.Point(18, 19);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(73, 17);
            this.lblIP.TabIndex = 1;
            this.lblIP.Text = "Ip number";
            // 
            // btnAdminExec
            // 
            this.btnAdminExec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdminExec.Location = new System.Drawing.Point(12, 163);
            this.btnAdminExec.Name = "btnAdminExec";
            this.btnAdminExec.Size = new System.Drawing.Size(297, 23);
            this.btnAdminExec.TabIndex = 3;
            this.btnAdminExec.Text = "Execute";
            this.btnAdminExec.UseVisualStyleBackColor = true;
            this.btnAdminExec.Click += new System.EventHandler(this.btnAdminExec_Click);
            // 
            // txtAdminCommands
            // 
            this.txtAdminCommands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdminCommands.Location = new System.Drawing.Point(12, 113);
            this.txtAdminCommands.Name = "txtAdminCommands";
            this.txtAdminCommands.Size = new System.Drawing.Size(297, 21);
            this.txtAdminCommands.TabIndex = 2;
            // 
            // cbxAdminCommands
            // 
            this.cbxAdminCommands.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxAdminCommands.FormattingEnabled = true;
            this.cbxAdminCommands.Items.AddRange(new object[] {
            "Drop Session",
            "Drop Extension",
            "Make Call"});
            this.cbxAdminCommands.Location = new System.Drawing.Point(12, 62);
            this.cbxAdminCommands.Name = "cbxAdminCommands";
            this.cbxAdminCommands.Size = new System.Drawing.Size(297, 23);
            this.cbxAdminCommands.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 11.25F);
            this.label5.Location = new System.Drawing.Point(103, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Admin commands";
            // 
            // logMessageDataGridViewTextBoxColumn
            // 
            this.logMessageDataGridViewTextBoxColumn.DataPropertyName = "logMessage";
            this.logMessageDataGridViewTextBoxColumn.HeaderText = "logMessage";
            this.logMessageDataGridViewTextBoxColumn.Name = "logMessageDataGridViewTextBoxColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 569);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.statusStrip1);
            this.MinimumSize = new System.Drawing.Size(691, 480);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tpStartpage.ResumeLayout(false);
            this.tpLogging.ResumeLayout(false);
            this.tpLogging.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLogging)).EndInit();
            this.tpAdmin.ResumeLayout(false);
            this.splitContainerAdmin.Panel2.ResumeLayout(false);
            this.splitContainerAdmin.ResumeLayout(false);
            this.splitContainerCommands.Panel1.ResumeLayout(false);
            this.splitContainerCommands.Panel2.ResumeLayout(false);
            this.splitContainerCommands.Panel2.PerformLayout();
            this.splitContainerCommands.ResumeLayout(false);
            this.splitContainerDetail.Panel1.ResumeLayout(false);
            this.splitContainerDetail.Panel1.PerformLayout();
            this.splitContainerDetail.Panel2.ResumeLayout(false);
            this.splitContainerDetail.Panel2.PerformLayout();
            this.splitContainerDetail.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnServerStart;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCloseConnections;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsServerIp;
        private System.Windows.Forms.ToolStripStatusLabel tsServerPort;
        private System.Windows.Forms.ToolStripStatusLabel tsServerPID;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpStartpage;
        private System.Windows.Forms.TabPage tpLogging;
        private System.Windows.Forms.TabPage tpAdmin;
        private System.Windows.Forms.ListBox lbData;
        private System.Windows.Forms.DataGridView gridLogging;
        private System.Windows.Forms.BindingSource dbLoggingDataSetBindingSource;
        private System.Windows.Forms.BindingSource dtLoggingBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn extensionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portSendingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn portReceivingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentSeqDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn connectedServerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn logMessageDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnGridRefresh;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDuurtot;
        private System.Windows.Forms.DateTimePicker dtpDuurvan;
        private System.Windows.Forms.TextBox tbSearchExtension;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.SplitContainer splitContainerAdmin;
        private System.Windows.Forms.SplitContainer splitContainerCommands;
        private System.Windows.Forms.SplitContainer splitContainerDetail;
        private System.Windows.Forms.ListBox lbSubEvents;
        private System.Windows.Forms.ListBox lbUncomMsg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSubEvents;
        private System.Windows.Forms.TextBox txtNrOfCalls;
        private System.Windows.Forms.Label lblNrOfCalls;
        private System.Windows.Forms.TextBox txtTimeOfFirstLogin;
        private System.Windows.Forms.Label lblFirstLoginTime;
        private System.Windows.Forms.Label lblOffice;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lblPersId;
        private System.Windows.Forms.TextBox txtOffice;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtPersId;
        private System.Windows.Forms.TextBox txtExtension;
        private System.Windows.Forms.Label lblExtension;
        private System.Windows.Forms.TextBox txtSeqNr;
        private System.Windows.Forms.TextBox txtPortReceivingR;
        private System.Windows.Forms.TextBox txtPortReceivingL;
        private System.Windows.Forms.TextBox txtPortSending;
        private System.Windows.Forms.TextBox txtPID;
        private System.Windows.Forms.Label lblSeqNr;
        private System.Windows.Forms.Label lblPortRecR;
        private System.Windows.Forms.Label lblPortRecL;
        private System.Windows.Forms.Label lblPortSend;
        private System.Windows.Forms.Label lblProcessID;
        private System.Windows.Forms.TextBox txtIPNumber;
        private System.Windows.Forms.Label lblIP;
        private System.Windows.Forms.Button btnAdminExec;
        private System.Windows.Forms.TextBox txtAdminCommands;
        private System.Windows.Forms.ComboBox cbxAdminCommands;
        private System.Windows.Forms.Label label5;
    }
}

