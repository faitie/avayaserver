﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Security.Cryptography;


namespace server
{
    public partial class Form1 : Form
    {
        ServerClasses.AvayaServer server;
        //dbLogger dbL = new dbLogger();
        int ListenPort;
        int iLog;
        //SQLiteDatabase db = new SQLiteDatabase();

        // This delegate enables asynchronous calls for setting 
        // the text property on a TextBox control. 
        delegate void SetTextCallback(string text);
        delegate void setGridUpdate();
        delegate void setTreeUpdate();
        delegate void setPhoneUpdate(List<string> data, Dictionary<string, bool> dicEvents, Dictionary<int, string> dicMsg);
        delegate void setSessionUpdate(List<string> data);

        public Form1()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {

        }

        void setFeedBack(string str)
        {
            if (lbData.InvokeRequired)
            {
                try
                {
                    SetTextCallback d = new SetTextCallback(SetText);
                    this.Invoke(d, new object[] { str + " (Invoke)" });
                }
                catch (Exception exc)
                {
                    dbLogger.Instance.addLog("Error on invoke : " + exc.ToString());

                }
            }
            else
            {
                lbData.Items.Insert(0, str);
            }
        }

        private void btnServerStart_Click(object sender, EventArgs e)
        {
            // instantiate new ipPhoneServer and listen on port 8333
            ListenPort = 8333;
            server = new ServerClasses.AvayaServer(ListenPort);

            // subscibe to ipPhoneServer events
            server.pushError += new IpPhoneServer.error(onPushError);
            server.pushMessage += new IpPhoneServer.message(onPushMessage);
            server.UpdateClients += new server.ServerClasses.AvayaServer.update(refreshAdminTree);
            server.parentNodeClicked += new ServerClasses.AvayaServer.parentNode(updatePhoneDetail);
            server.childNodeClicked += new ServerClasses.AvayaServer.childNode(updateSessionDetail);

            statusStrip1.Items[0].Text = "Server IP : " + GetLocalIP();
            statusStrip1.Items[1].Text = "Server Port : " + ListenPort.ToString();
            statusStrip1.Items[2].Text = "Server PID : " + Process.GetCurrentProcess().Id.ToString();
            
            lbData.Items.Add("SERVER STARTED");
        }

        private void SetText(string text)
        {
            this.lbData.SuspendLayout();
            iLog++;
            if (iLog >= 500)
            {
                //add to external Log
                copyLogToTxt(lbData);
                //remove listbox
                lbData.Items.Clear();
            }
            this.lbData.Items.Insert(0, text);
            this.lbData.ResumeLayout();
            this.refreshAdminTree();
        }

        private void copyLogToTxt(ListBox lb)
        {
            //LogWriter writer = LogWriter.Instance;
            //writer.logName("FormLog");
            //foreach (string s in lb.Items)
            //{
            //    writer.WriteToLog(s);
            //}
        }

        //private void send(TcpClient tcpClient, string str)
        //{
        //    NetworkStream clientStream = tcpClient.GetStream();
        //    ASCIIEncoding encoder = new ASCIIEncoding();
        //    byte[] buffer = encoder.GetBytes(str);

        //    clientStream.Write(buffer, 0, buffer.Length);
        //    clientStream.Flush();
        //}

        private void onPushError(string str)
        {
            setFeedBack(str);
        }

        private void onPushMessage(string str)
        {
            setFeedBack(str);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (server != null)
            {
                copyLogToTxt(lbData);
                server.closeServer();
                server = null;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if(server != null)
            {
                server.getPhoneSessions();
            }
        }

        private void btnCloseConnections_Click(object sender, EventArgs e)
        {
            server.closeConnections();
        }

        private string GetLocalIP()
        {
            string _IP = null;
            // Resolves a host name or IP address to an IPHostEntry instance.
            // IPHostEntry - Provides a container class for Internet host address information. 
            System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            // IPAddress class contains the address of a computer on an IP network.
            foreach (System.Net.IPAddress _IPAddress in _IPHostEntry.AddressList)
            {
                // InterNetwork indicates that an IP version 4 address is expected 
                // when a Socket connects to an endpoint
                if (_IPAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    _IP = _IPAddress.ToString();
                }
            }
            return _IP;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            lbData.Height = this.Height - 177;
            gridLogging.Height = this.Height - 177;
            //splitContainerAdmin.SplitterDistance = 290;
            
            //listboxes
            lbSubEvents.Width = (splitContainerDetail.Panel2.Width / 2) - 50;
            lbUncomMsg.Width = (splitContainerDetail.Panel2.Width / 2) - 50;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.refreshGrid();
        }

        public void refreshGrid()
        {
            // TODO: This line of code loads data into the 'dbLoggingDataSet.allLogging' table. You can move, or remove it, as needed.
            //this.allLoggingTableAdapter.Fill(this.dbLoggingDataSet.allLogging);
            if (gridLogging.InvokeRequired)
            {
                try
                {
                    setGridUpdate gridUp = new setGridUpdate(setGrid);
                    this.Invoke(gridUp);
                }
                catch(Exception exc)
                {
                    dbLogger.Instance.addLog("Error on refreshGrid Invoke : " + exc.ToString());
                }
            }
            else
            {
                setGrid();
            }
        }

        private void setGrid()
        {
            string query;
            

            try
            {
                this.gridLogging.SuspendLayout();
                query = "SELECT extension,ts,log FROM dtLogging ORDER BY ts desc";
                gridLogging.DataSource = dbLogger.Instance.getQuery(query); //db.GetDataTable(query); 
                gridLogging.Columns["log"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                gridLogging.ResumeLayout();
            }
            catch(Exception exc) 
            {
                dbLogger.Instance.addLog("Error on set grid : " + exc.ToString());
            }
        }

        public void refreshAdminTree()
        {
            if (splitContainerAdmin.InvokeRequired)
            {
                try
                {
                    setTreeUpdate treeUp = new setTreeUpdate(setAdminTree);
                    this.Invoke(treeUp);
                }
                catch(Exception exc)
                {
                    dbLogger.Instance.addLog("Error on refreshAdminTree Invoke : " + exc.ToString());
                }
            }
            else
            {
                setAdminTree();
            }
        }

        private void setAdminTree()
        {
            try
            {
                if (server.refreshAdminTree() != null)
                {
                    this.splitContainerAdmin.Panel1.Controls.Add(server.refreshAdminTree());
                }
                else { cleanDetail(); }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("Error in setAdminTree : " + exc.ToString());
            }
        }

        private void updatePhoneDetail(List<string> data, Dictionary<string,bool> dicEvents, Dictionary<int,string>dicMsg)
        {
            if (splitContainerDetail.InvokeRequired)
            {
                try
                {
                    setPhoneUpdate phoneUp = new setPhoneUpdate(setPhoneDetail);
                    this.Invoke(phoneUp, new object[] { data, dicEvents, dicMsg });
                }
                catch(Exception exc)
                {
                    dbLogger.Instance.addLog("Error update phone detail : " + exc.ToString());
                }
            }
            else
            {
                setPhoneDetail(data, dicEvents, dicMsg);
            }
        }

        private void setPhoneDetail(List<string> data, Dictionary<string, bool> dicEvents, Dictionary<int, string> dicMsg)
        {
            try
            {
                this.txtExtension.Text = data[0];
                this.txtPersId.Text = data[1];
                this.txtName.Text = data[2];
                this.txtOffice.Text = data[3];

                this.lbSubEvents.Items.Clear();
                this.lbUncomMsg.Items.Clear();
                foreach (KeyValuePair<string, bool> eventPair in dicEvents)
                {
                    this.lbSubEvents.Items.Add(eventPair.Key.ToString() + " - " + eventPair.Value.ToString());
                }
                foreach (KeyValuePair<int, string> msgPair in dicMsg)
                {
                    this.lbUncomMsg.Items.Add(msgPair.Key.ToString() + " - " + msgPair.Value.ToString());
                }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(exc.ToString());
            }
        }

        private void updateSessionDetail(List<string> data)
        {
            if (splitContainerDetail.InvokeRequired)
            {
                try
                {
                    setSessionUpdate sessionUp = new setSessionUpdate(setSessionDetail);
                    this.Invoke(sessionUp, new object[] { data });
                }
                catch
                {
                    MessageBox.Show("Error update session detail");
                }
            }
            else
            {
                setSessionDetail(data);
            }
        }

        private void setSessionDetail(List<string> data)
        {
            this.txtIPNumber.Text = data[0];
            this.txtPID.Text = data[1];
            this.txtPortSending.Text = data[2];
            this.txtPortReceivingL.Text = data[3];
            this.txtPortReceivingR.Text = data[4];
            this.txtSeqNr.Text = data[5];
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            refreshGrid();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string query;

            if (tbSearchExtension.Text.Length != 0)
            {
                query = "SELECT extension,ts,log FROM dtLogging WHERE extension = '" + tbSearchExtension.Text + "' AND ts >= '" + DateTime.Parse(dtpDuurvan.Text.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' AND ts <= '" + DateTime.Parse(dtpDuurtot.Text.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY ts desc";
            }
            else
            {
                query = "SELECT extension,ts,log FROM dtLogging WHERE ts >= '" + DateTime.Parse(dtpDuurvan.Text.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' AND ts <= '" + DateTime.Parse(dtpDuurtot.Text.ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "' ORDER BY ts desc";
            }

            try
            {
                gridLogging.DataSource = dbLogger.Instance.getQuery(query);
                gridLogging.Columns["log"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                gridLogging.Refresh();
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(exc.ToString());
            }
        }

        private void cleanDetail()
        {
            this.txtExtension.Text = "";
            this.txtPersId.Text = "";
            this.txtName.Text = "";
            this.txtOffice.Text = "";

            this.txtIPNumber.Text = "";
            this.txtPID.Text = "";
            this.txtPortSending.Text = "";
            this.txtPortReceivingL.Text = "";
            this.txtPortReceivingR.Text = "";
            this.txtSeqNr.Text = "";

            this.lbSubEvents.Items.Clear();
            this.lbUncomMsg.Items.Clear();
        }

        private void btnAdminExec_Click(object sender, EventArgs e)
        {
            string[] data = null ;
            string cmd = cbxAdminCommands.SelectedItem.ToString();

            switch (cmd)
            {
                case "Drop Session":
                    data = new string[2];
                    data[0] = txtIPNumber.Text;
                    data[1] = txtPID.Text;
                    break;
                case "Drop Extension":
                    data = new string[0];
                    break;
                    
            }
            server.adminExec(txtExtension.Text, cmd, data);
        }

    }
}
