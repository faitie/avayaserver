﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace server.ServerClasses
{
    class AvayaServer:IpPhoneServer
    {
        //Class is soly used for graphical feedback.
        //All the avaya intel is in the IpPhoneServer, IpPhone en IpPhoneSession classes

        private TreeView tree;        

        // EVENTS
        public event parentNode parentNodeClicked;
        public delegate void parentNode(List<string> lData,Dictionary<string,bool>dicEvents,Dictionary<int,string>dicMsg);

        public event childNode childNodeClicked;
        public delegate void childNode(List<string> lData);

        public event update UpdateClients;
        public delegate void update();

        // CONSTRUCTOR
        public AvayaServer(int _listenPort)
            : base(_listenPort)
        {
            base.updateClients += new newcon(AvayaServer_updateClients);
        }

        //DESTRUCTOR
        ~AvayaServer()
        {
            
        }
        
        public void adminExec(string sExtension, string sCommand, string[] sData)
        {
            IpPhone phone = null;
            phone = dicPhone[sExtension];

            switch(sCommand)
            {
                case "Drop Session":
                    string key = sData[0] + ";" + sData[1].ToString();
                    phone.sendProtocol("FIN", "", "", phone.dicIpPhoneSessions[key]);
                    phone.removeSession(sData[0], sData[1]);
                    if (phone.sessionCount() <= 0) { dicPhone.Remove(sExtension); }
                    break;
                case "Drop Extension":
                    phone.sendProtocol("FIN", "", "");
                    foreach (KeyValuePair<string, IpPhoneSession> session in phone.dicIpPhoneSessions)
                    {
                        session.Value.closePhoneSession();
                    }
                    phone.closePhone();
                    dicPhone.Remove(sExtension);
                    break;
            }
            AvayaServer_updateClients();
        }

        private void tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            IpPhone phone = null;
            IpPhoneSession session = null;
            TreeNode node = null;
            List<string> phoneData = new List<string>();
            List<string> sessionData = new List<string>();

            node = e.Node.Parent;

            // als er geen parent is wil dit zeggen dat we
            // al een parent node vast hebben met een extension.
            if (node == null)
            {
                try
                { phone = dicPhone[e.Node.Text.ToString()]; }
                catch (Exception exc)
                {
                    //error.
                    phone = null;
                }
            }
            else
            {
                try
                {
                    // child node
                    phone = dicPhone[node.Text.ToString()];
                    session = phone.dicIpPhoneSessions[e.Node.Text.ToString()];

                    sessionData.Add(session.ipNumber.ToString());
                    sessionData.Add(session.processID.ToString());
                    sessionData.Add(session.portSending.ToString());
                    sessionData.Add(session.portReceiving.ToString());
                    sessionData.Add(session.portReceivingRemote.ToString());
                    sessionData.Add(session.currentSequenceNumber.ToString());

                    this.childNodeClicked(sessionData);
                }
                catch (Exception exc)
                {
                    dbLogger.Instance.addLog("AvayaServer - tree_nodeMouseCLick : " + exc.ToString());
                }
            }
            if (phone != null)
            {
                phoneData.Add(phone.extension.ToLower());
                phoneData.Add(phone.persID.ToLower());
                phoneData.Add(phone.name.ToLower());
                phoneData.Add(phone.office.ToLower());
                // nog peroonlijke data toevoegen 
                // 1ste login tijd
                // nr of calls
                this.parentNodeClicked(phoneData, phone.dicSubscribedEvents, phone.dicUncommitedMessages);
            }
        }

        public TreeView refreshAdminTree()
        {
            int i = 0;
            if (tree == null) { tree = new TreeView(); }

            tree.Nodes.Clear();
            tree.BeginUpdate();
            foreach (KeyValuePair<string, IpPhone> phonePair in dicPhone)
            {
                tree.Nodes.Add(phonePair.Key.ToUpper());
                foreach (KeyValuePair<string, IpPhoneSession> sessionPair in phonePair.Value.dicIpPhoneSessions)
                {
                    tree.Nodes[i].Nodes.Add(sessionPair.Key.ToLower());
                }
                i++;
            }
            // events
            tree.NodeMouseClick += new TreeNodeMouseClickEventHandler(tree_NodeMouseClick);


            // layout
            tree.ExpandAll();
            tree.Dock = DockStyle.Fill;
            tree.EndUpdate();
            return tree;
        }

        public void AvayaServer_updateClients()
        {
            this.UpdateClients();
        }

    }
}
