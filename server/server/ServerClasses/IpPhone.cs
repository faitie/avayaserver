﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Data;
using System.Diagnostics;
using Newtonsoft.Json;

namespace server
{
    class IpPhone
    {
        //VARIABLES
        private IpPhoneSession phoneSession;
        private objAvaya avaya;
        private TcpClient tcpClient;
        private NetworkStream clientStream;
        public Dictionary<string, IpPhoneSession> dicIpPhoneSessions;
        private bool setAuxWork = false;
        /*private dbLogger dbL;*/

        // PROPERTIES
        public string extension = "";
        public string ipNumber;
        public int processID;
        public int portSending;
        public int portReceiving;
        public string persID;
        public string name;
        public string office;
        public int currentSequenceNumber;
        public IpPhoneServer server;

        public Dictionary<string, bool> dicSubscribedEvents;
        public Dictionary<int, string> dicUncommitedMessages;
                
        // EVENTS
        public event message pushMessage;
        public delegate void message(string str);

        public event error pushError;
        public delegate void error(string str);

        public event onSessionAdded sessionAdded;
        public delegate void onSessionAdded();

        // CONSTRUCTOR
        public IpPhone(string sExtension)
        {
            extension = sExtension;
            dicIpPhoneSessions = new Dictionary<string, IpPhoneSession>();

            dicSubscribedEvents = new Dictionary<string, bool>();
            dicUncommitedMessages = new Dictionary<int, string>();
        }

        // DESTRUCTOR
        ~IpPhone()
        {
            closePhone();
        }

        // METHODS
        // Public methods
        public void startAvayaConnection()
        {
            // instantiate new avaya object
            try
            {
                avaya = new objAvaya(extension);

                avaya.pushMessage += new objAvaya.message(onPushMessage);
                avaya.pushError += new objAvaya.error(onPushError);
                avaya.pushLampMode += new objAvaya.onGetLampMode(onPushLampMode);
                avaya.pushTerminalRegistered += new objAvaya.onTerminalRegistered(onPushTerminalRegistered);
                avaya.onIncommingCall += new objAvaya.incommingCall(onIncommingCall);

                avaya.startSession();

                subscribeEvent("inboundCalls", true);
                subscribeEvent("outboundCalls", true);

            }
            catch(Exception exc)
            {
                dbLogger.Instance.addLog(this, "Error on instanciating avaya object : " + exc.ToString());
            }
        }

        public void addThreadingObjects(TcpClient _myTcpCLient, NetworkStream _myNetworkStream)
        {
            tcpClient = _myTcpCLient;
            clientStream = _myNetworkStream;
        }

        public void checkSession(string _ipNumber, int _ProcessID, int _portSending, int _portReceiving, int _currentSeq)
        {
            ipNumber = _ipNumber;
            processID = _ProcessID;
            portSending = _portSending;
            portReceiving = _portReceiving;
            currentSequenceNumber = _currentSeq;

            string sessionKey = _ipNumber + ";" + _ProcessID.ToString();

            try
            {
                if (!dicIpPhoneSessions.ContainsKey(sessionKey))
                {
                    currentSequenceNumber = _currentSeq;

                    addSession();
                }
                else
                {
                    // data to log
                }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void addSession()
        {
            try
            {
                string sessionKey = ipNumber + ";" + processID.ToString();
                IPEndPoint IPE = (IPEndPoint)tcpClient.Client.RemoteEndPoint;

                // instansiate new ipPhoneSession for the phone class
                phoneSession = new IpPhoneSession(ipNumber, processID, portSending, portReceiving, IPE.Port, currentSequenceNumber, tcpClient, clientStream, this);

                // add phoneSession to dictionary
                dicIpPhoneSessions.Add(sessionKey, phoneSession);

                // insert record in the datatable
                //dtIpPhone.Rows.Add(ipNumber, processID, portSending, portReceiving, currentSequenceNumber, avaya.sServerIp);

                // data to log

                // publish on adding of a new session.
                this.sessionAdded();

                this.sendProtocol("ACK");
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }
        
        public void setAux(bool auxWorkOn)
        {
            try
            {
                setAuxWork = auxWorkOn;
                avaya.checkAuxWork();
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(this, "IpPhone - setAux : " + exc.ToString());
            }
        }

        public void makeCall(string telNr)
        {
            try
            {
                avaya.makeCall(telNr);
            }
            catch (Exception exc)
            {
               dbLogger.Instance.addLog(this, "IpPhone - makeCall : " + exc.ToString());
            }
        }

        public void transferTo(string telNr, bool directMode)
        {
            try { avaya.transferCall(telNr, directMode); }
            catch (Exception exc) { dbLogger.Instance.addLog("IpPhone - transferTo : " + exc.ToString()); }
        }

        public void subscribeEvent(string strEvent, bool blnEvents)
        {
            try
            {
                if (!dicSubscribedEvents.ContainsKey(strEvent))
                {
                    dicSubscribedEvents.Add(strEvent, blnEvents);
                    avaya.subscribeEvent(strEvent, blnEvents);
                }
                else
                {
                    if (dicSubscribedEvents[strEvent] != blnEvents)
                    {
                        dicSubscribedEvents[strEvent] = blnEvents;
                        avaya.subscribeEvent(strEvent, blnEvents);
                    }
                }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("IpPhone - subscribeEvent : " + exc.ToString());
            }
        }

        public void endActiveCall()
        {
            avaya.endCall();
        }

        public bool removeSession(string ip, string id)
        {
            string sessionKey = ip + ";" + id;

            bool keepAlive = true;
            try
            {
                keepAlive = dicIpPhoneSessions[sessionKey].closePhoneSession();
                dicIpPhoneSessions.Remove(sessionKey);

                if (dicIpPhoneSessions.Count == 0)
                {
                    closePhone();
                }
                this.server.updateTree();
            }
            catch(Exception exc)
            {
                dbLogger.Instance.addLog(this, exc.ToString());
            }
            return keepAlive;
        }

        public bool removeSession(string ip, int id)
        {
            string sessionKey = ip + ";" + id.ToString();

            bool keepAlive = true;
            try
            {
                keepAlive = dicIpPhoneSessions[sessionKey].closePhoneSession();
                dicIpPhoneSessions.Remove(sessionKey);


                if (dicIpPhoneSessions.Count == 0)
                {
                    closePhone();
                }
                this.server.updateTree();
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(this, exc.ToString());
            }
            return keepAlive;
        }

        public int sessionCount()
        {
            return dicIpPhoneSessions.Count;
        }

        public void closePhone()
        {
            if (phoneSession != null)
            {
                phoneSession = null;
            }
            if (dicIpPhoneSessions != null)
            {
                foreach (KeyValuePair<string, IpPhoneSession> sessionPair in dicIpPhoneSessions)
                {
                    sessionPair.Value.closePhoneSession();
                }
                dicIpPhoneSessions.Clear();
            }
            if (dicSubscribedEvents != null)
            {
                dicSubscribedEvents.Clear();
                dicSubscribedEvents = null;
            }
            if (dicUncommitedMessages != null)
            {
                dicUncommitedMessages.Clear();
                dicUncommitedMessages = null;
            }
            if (avaya != null)
            {
                avaya.stopSession();
                avaya = null;
            }
            this.server.updateTree();
        }

        public void cleanup()
        {
            phoneSession = null;
            avaya = null;
            tcpClient = null;
            clientStream = null;
            dicIpPhoneSessions = null;
            extension = "";

            ipNumber = "";
            processID = 0;
            portSending = 0;
            portReceiving = 0;
            currentSequenceNumber = 0;
        }

        // Private methods
        public void sendProtocol(string tos)
        {
            try
            {
                sendProtocol(tos, "");
            }
            catch (Exception exc)
            {
                //pushError("sendProtocol exception : " + exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void sendProtocol(string tos, string data)
        {
            try
            {
                sendProtocol(tos, "", data);
            }
            catch (Exception exc)
            {
                //pushError("sendProtocol exception : " + exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void sendProtocol(string tos, string cmd, string data)
        {
            try
            {
                sendMessage(tos, cmd, data);
            }
            catch (Exception exc)
            {
                //pushError("sendProtocol exception : " + exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void sendProtocol(string tos, string cmd, string data, IpPhoneSession session)
        {
            sendMessage(tos, cmd, data, session);
        }

        public void sendMessage(string tos, string cmd, string data, IpPhoneSession session)
        {
            DestinationID desID = new DestinationID();
            desID.ipNumber = "";
            desID.phoneNumber = "";
            desID.portReceiving = 0;
            desID.portSending = 0;
            desID.processID = 0;

            SourceID sID = new SourceID();
            sID.ipNumber = GetLocalIP();
            sID.phoneNumber = "";
            sID.portReceiving = 0;
            sID.portSending = portReceiving;
            sID.processID = Process.GetCurrentProcess().Id;

            List<DestinationID> lDesId = new List<DestinationID>();
            lDesId.Add(desID);

            List<SourceID> lsID = new List<SourceID>();
            lsID.Add(sID);

            TtJSON tt = new TtJSON();
            tt.signature = "ipPhoneV1";
            tt.sourceID = lsID;
            tt.destinationID = lDesId;
            tt.sequenceID = 1;
            tt.TOS = tos + ":" + cmd;
            tt.data = data;

            List<TtJSON> lsTT = new List<TtJSON>();
            lsTT.Add(tt);

            DsJSON ds = new DsJSON();
            ds.ttJSON = lsTT;

            RootObject root = new RootObject();
            root.dsJSON = ds;

            string lcOut = JsonConvert.SerializeObject(root);

            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = encoder.GetBytes(lcOut);

            try
            {
                session.SessionStream.Write(buffer, 0, buffer.Length);
                session.SessionStream.Flush();
            }
            catch (Exception exc)
            {
                //pushError("sendMessage : " + exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void sendMessage(string tos, string cmd, string data)
        {
            DestinationID desID = new DestinationID();
            desID.ipNumber = "";
            desID.phoneNumber = "";
            desID.portReceiving = 0;
            desID.portSending = 0;
            desID.processID = 0;

            SourceID sID = new SourceID();
            sID.ipNumber = GetLocalIP();
            sID.phoneNumber = "";
            sID.portReceiving = 0;
            sID.portSending = portReceiving;
            sID.processID = Process.GetCurrentProcess().Id;

            List<DestinationID> lDesId = new List<DestinationID>();
            lDesId.Add(desID);

            List<SourceID> lsID = new List<SourceID>();
            lsID.Add(sID);

            TtJSON tt = new TtJSON();
            tt.signature = "ipPhoneV1";
            tt.sourceID = lsID;
            tt.destinationID = lDesId;
            tt.sequenceID = 1;
            tt.TOS = tos + ":" + cmd;
            tt.data = data;

            List<TtJSON> lsTT = new List<TtJSON>();
            lsTT.Add(tt);

            DsJSON ds = new DsJSON();
            ds.ttJSON = lsTT;

            RootObject root = new RootObject();
            root.dsJSON = ds;

            string lcOut = JsonConvert.SerializeObject(root);

            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = encoder.GetBytes(lcOut);

            foreach (KeyValuePair<string, IpPhoneSession> sessionPair in dicIpPhoneSessions)
            {
                phoneSession = sessionPair.Value;

                try
                {
                    phoneSession.SessionStream.Write(buffer, 0, buffer.Length);
                    phoneSession.SessionStream.Flush();
                    
                }
                catch (Exception exc)
                {
                    //pushError("sendMessage : " + exc.ToString());
                    dbLogger.Instance.addLog(this, exc.ToString());
                }
            }  
        }

        private string GetLocalIP()
        {
            string _IP = null;
            // Resolves a host name or IP address to an IPHostEntry instance.
            // IPHostEntry - Provides a container class for Internet host address information. 
            System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            // IPAddress class contains the address of a computer on an IP network.
            foreach (System.Net.IPAddress _IPAddress in _IPHostEntry.AddressList)
            {
                // InterNetwork indicates that an IP version 4 address is expected 
                // when a Socket connects to an endpoint
                if (_IPAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    _IP = _IPAddress.ToString();
                }
            }
            return _IP;
        }

        // events
        private void onPushError(string str)
        {
            //this.pushError(str);
            dbLogger.Instance.addLog(this, str);
        }

        private void onPushMessage(string str)
        {
            this.pushMessage(str);
            dbLogger.Instance.addLog(this, str);
        }

        private void onPushLampMode(bool bln)
        {
            if (bln != setAuxWork)
            {
                try { avaya.setButton(19); }
                catch (Exception exc) { dbLogger.Instance.addLog("IpPhone - onPushLampMode : " + exc.ToString()); }
            }
        }

        private void onPushTerminalRegistered(bool bln)
        {
            try
            {
                this.setAux(false);
            }
            catch (Exception exc)
            { dbLogger.Instance.addLog("IpPhone - onPushTerminalRegistered : " + exc.ToString()); }
        }

        private void onIncommingCall(string sIncommingCallId)
        {
            try
            {
                this.sendProtocol("EVT", "incomingcall", sIncommingCallId);
            }
            catch (Exception exc)
            { dbLogger.Instance.addLog("IpPhone - onIncommingCall : " + exc.ToString()); }
        }

    }
}
