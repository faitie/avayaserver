﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace server
{
    class IpPhoneServer
    {
        private TcpListener tcpListener;
        private TcpClient client;
        private Thread listenThread;
        private IpPhone phone;
        public ServerClasses.AvayaServer parent;
        
        public Dictionary<string, IpPhone> dicPhone = new Dictionary<string, IpPhone>();

        private Semaphore semPhone = new Semaphore(1, 1);

        private int listenPort;
        private bool listen;

        private string cError = "";

        //private DataTable dtIpPhoneServer;


        // EVENTS
        public event message pushMessage;
        public delegate void message(string str);

        public event error pushError;
        public delegate void error(string str);

        protected event newcon updateClients;
        protected delegate void newcon();

        public event newLog updateDbLog;
        public delegate void newLog();

        // CONSTRUCTOR
        protected IpPhoneServer(int _listenPort)
        {
            listenPort = _listenPort;
            // setup datatable
            //dtIpPhoneServer = new DataTable("IpPhoneServer");
            //dtIpPhoneServer.Columns.Add("extension", typeof(string));
            //dtIpPhoneServer.Columns.Add("ipNumber", typeof(string));
            //dtIpPhoneServer.Columns.Add("processID", typeof(int));
            //dtIpPhoneServer.Columns.Add("portSending", typeof(int));
            //dtIpPhoneServer.Columns.Add("portReceiving", typeof(int));
            //dtIpPhoneServer.Columns.Add("currentSequenceNumber", typeof(int));
            //dtIpPhoneServer.Columns.Add("connectedServer", typeof(string));

            // setup database logger
            //dbL = new dbLogger();
            //dbL.addLog(this, "SERVER STARTED");


            // create new tcpListener and create single thread for listening
            // to new connections

            listen = true;

            this.tcpListener = new TcpListener(System.Net.IPAddress.Any, listenPort);
            this.listenThread = new Thread(new ThreadStart(listenForConnections));
            this.listenThread.Name = "MainListenThread";
            this.listenThread.Start();
        }
        
        // DESTRUCTOR
        ~IpPhoneServer()
        {
            closeServer();
            //if (dbL != null) { dbL = null; }
        }

        // METHODS
        /* PUBLIC */
        public void closeServer()
        {
            try
            {
                Thread export = new Thread(new ThreadStart(this.getPhoneSessions));
                export.Start();
                if (listenThread != null) { listenThread.Abort(); }
                if (tcpListener != null) { listen = false; tcpListener.Stop(); }
                if (client != null) { client.Close(); }
                closeConnections();
                export.Join();
                dicPhone = null;
            }
            catch (Exception exc)
            {
                //pushError("on Close Server : " + exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
            }
        }

        public void closeConnections()
        {
            if (dicPhone != null)
            {
                // Loop over pairs with foreach
                foreach (KeyValuePair<string, IpPhone> phonePair in dicPhone)
                {
                    phonePair.Value.closePhone();
                }
                dicPhone.Clear();
            }
        }

        public void getPhoneSessions()
        {
            
        }

        public void delPhoneFromDic(string extension)
        {
            try{ dicPhone.Remove(extension); }
            catch (Exception exc) { dbLogger.Instance.addLog("IpPhoneServer - delPhoneFromDic : " + exc.ToString()); }
        }

        public void updateTree()
        {
            this.updateClients();
        }

        /* PRIVATE */
        private void listenForConnections()
        {
            if(listen) {this.tcpListener.Start();}

            while (listen)
            {
                try
                {
                    //blocks until a client has connected to the server
                    client = this.tcpListener.AcceptTcpClient();

                    //create a thread to handle communication 
                    //with connected client
                    ThreadPool.QueueUserWorkItem(new WaitCallback(handleConnections),client);

                    //data to log
                }
                catch (Exception exc)
                {
                    cError = "";
                    cError = "Create Thread To Handle Communication With Connected Client : " + exc.ToString();
                    dbLogger.Instance.addLog(this, cError);
                }
            }
        }
        
        private void handleConnections(object tcp)
        {
            bool keepClient;
            byte[] message = new byte[4096];
            int bytesRead;
            string incomingData;

            //_myObject uitleden om de interne objecten te gebruiken.
            TcpClient tcpClient = tcp as TcpClient;
            NetworkStream clientStream = tcpClient.GetStream();

            keepClient = true;

            while (keepClient)
            {
                bytesRead = 0;

                try
                {
                    //blocks until a client sends a message
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch(Exception exc)
                {
                    //a socket error has occured
                    dbLogger.Instance.addLog(this, exc.ToString());
                    break;
                }

                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                incomingData = encoder.GetString(message, 0, bytesRead);

                //building clean JSON string
                incomingData = incomingData.Replace("/n", string.Empty);

                //interpret incoming data
                keepClient = checkConnection(incomingData, tcpClient, clientStream);
            }
        }

        private bool checkConnection(string inc, TcpClient _tcpClient, NetworkStream _networkStream)
        {
            char[] sep = {';'};
            RootObject JSON = null;
            Dictionary<string, string> JSONDECODE = null;

            try
            {
                 JSON = JsonConvert.DeserializeObject<RootObject>(inc);
                 JSONDECODE  = deCode(JSON);
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog(this, exc.ToString());
            }

            bool keepAlive = true;

            try
            {
                switch (JSONDECODE["TOS_type"])
                {
                    case "CMD":
                        switch (JSONDECODE["TOS_cmd"])
                        {
                            case "register":
                                keepAlive = createPhone(JSON, _tcpClient, _networkStream);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("IpPhoneServer - checkReceived : " + exc.ToString());
            }
            return keepAlive;
        }

        private Dictionary<string,string> deCode(RootObject inc)
        {
            Dictionary<string, string> output = new Dictionary<string, string>();
            string command = "";
            string[] sOut = null;
            char[] sep = { ':' };

            try
            {
                command = inc.dsJSON.ttJSON[0].TOS;
                sOut = command.Split(sep);
            }
            catch(Exception exc)
            { dbLogger.Instance.addLog("ipPhoneServer - deCode : " + exc.ToString()); }

            try
            {
                output["signature"] = inc.dsJSON.ttJSON[0].signature;
                output["sID_phonenumber"] = inc.dsJSON.ttJSON[0].sourceID[0].phoneNumber;
                output["sID_ipnumber"] = inc.dsJSON.ttJSON[0].sourceID[0].ipNumber;
                output["sID_processid"] = inc.dsJSON.ttJSON[0].sourceID[0].processID.ToString();
                output["sID_portreceiving"] = inc.dsJSON.ttJSON[0].sourceID[0].portReceiving.ToString();
                output["sID_portsending"] = inc.dsJSON.ttJSON[0].sourceID[0].portSending.ToString();
                output["dID_phonenumber"] = inc.dsJSON.ttJSON[0].destinationID[0].phoneNumber;
                output["dID_ipnumber"] = inc.dsJSON.ttJSON[0].destinationID[0].ipNumber;
                output["dID_processid"] = inc.dsJSON.ttJSON[0].destinationID[0].processID.ToString();
                output["dID_portreceiving"] = inc.dsJSON.ttJSON[0].destinationID[0].portReceiving.ToString();
                output["dID_portsending"] = inc.dsJSON.ttJSON[0].destinationID[0].portSending.ToString();
                output["seqID"] = inc.dsJSON.ttJSON[0].sequenceID.ToString();
                output["TOS_type"] = sOut[0];
                output["TOS_cmd"] = sOut[1];
                output["data"] = inc.dsJSON.ttJSON[0].data.ToString();
            }
            catch (Exception exc)
            { dbLogger.Instance.addLog("ipPhoneServer - deCode : " + exc.ToString()); }

            return output;
        }

        private bool createPhone(RootObject incJSON, TcpClient _tcpClient, NetworkStream _networkStream)
        {
            bool returnValue = false;
            char[] sepChar = {';'};
            string[] myData = null;
            string myExtension = "";
            string myPersID = "";
            string myName = "";
            string myOffice = "";
            // set semaphore to wait for release of method.
            // this way only 1 thread can acces register terminal at a time.
            // reduces risk of overload and overwriting variables
            semPhone.WaitOne();

            try
            {
                try
                {
                    myData = incJSON.dsJSON.ttJSON[0].data.Split(sepChar);
                    myExtension = myData[0].ToString();
                    myPersID = myData[1].ToString();
                    myName = myData[2].ToString();
                    myOffice = myData[3].ToString();
                }
                catch
                {
                    /*dbL.addLog(this, "Cant decode create Phone Data string");*/
                }

                string myIpNumber = incJSON.dsJSON.ttJSON[0].sourceID[0].ipNumber;
                int myProcessID = incJSON.dsJSON.ttJSON[0].sourceID[0].processID;
                int myPortSending = incJSON.dsJSON.ttJSON[0].sourceID[0].portSending;
                int myPortReceiving = incJSON.dsJSON.ttJSON[0].sourceID[0].portReceiving;
                int mySequenceNumber = incJSON.dsJSON.ttJSON[0].sequenceID;

                if (!dicPhone.ContainsKey(myExtension))
                {
                    // per new connection check if extension is already registered
                    // if not registered create new IpPhone instance
                    //if (phone != null) { phone.cleanup();  phone = null; }
                    phone = new IpPhone(myExtension);

                    // subscribe to ipPhone events
                    phone.pushError += new IpPhone.error(onPushError);
                    phone.pushMessage += new IpPhone.message(onPushMessage);
                    phone.sessionAdded += new IpPhone.onSessionAdded(phone_sessionAdded);

                    // set phone variables
                    phone.ipNumber = myIpNumber;
                    phone.processID = myProcessID;
                    phone.portSending = myPortSending;
                    phone.portReceiving = myPortReceiving;
                    phone.currentSequenceNumber = mySequenceNumber;
                    phone.persID = myPersID;
                    phone.name = myName;
                    phone.office = myOffice;
                    phone.addThreadingObjects(_tcpClient, _networkStream);
                    // adding new phonesession + insert into dictionary
                    phone.checkSession(myIpNumber, myProcessID, myPortSending, myPortReceiving, mySequenceNumber);
                    phone.server = this;

                    // add new IpPhone instance to phone dictionary
                    dicPhone.Add(myExtension, phone);
                                                            
                    //data to log
                }
                else
                {
                    // get phone instance of the extension
                    phone = dicPhone[myExtension];
                    // check if session already exists if not add new session
                    // else write to log that a double session was trying to spawn
                    phone.addThreadingObjects(_tcpClient, _networkStream);
                    phone.checkSession(myIpNumber, myProcessID, myPortSending, myPortReceiving, mySequenceNumber);
                }
                returnValue = false;
            }
            catch (Exception exc)
            {
                //pushError(exc.ToString());
                dbLogger.Instance.addLog(this, exc.ToString());
                returnValue = true;
            }
            finally
            {
                // release semaphore
                // lets next in queue enter method.
                semPhone.Release();
            }
            return returnValue;
        }
        
        // EVENT METHODS
        protected void phone_sessionAdded()
        {
            this.updateClients();
        }

        public void onPushError(string str)
        {
            this.pushError(str);
        }

        public void onPushMessage(string str)
        {
            this.pushMessage(str);
        }

    }

}

