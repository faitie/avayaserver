﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using System.Timers;

namespace server
{
    class IpPhoneSession
    {
        // VARIABLES
        public string ipNumber;
        public int processID;
        public int portSending;
        public int portReceiving;
        public int portReceivingRemote;
        public int currentSequenceNumber;
        public bool keepAlive;

        public TcpClient tcpSession;
        public NetworkStream SessionStream;
        public Thread SessionThread;
        public IpPhone p;
        private System.Timers.Timer syncTimer;

        private int iSync;
        private string sessionKey;

        // PROPERTIES
        public int timeOutTime;

        // CONSTRUCTOR
        public IpPhoneSession(string _ipNumber, int _processID, int _portSending, int _portReceiving, int _portReceivingRemote, int _currentSequenceNumber, TcpClient _tcpClient, NetworkStream _clientSteam, IpPhone _p)
        {
            ipNumber = _ipNumber;
            processID = _processID;
            portSending = _portSending;
            portReceiving = _portReceiving;
            portReceivingRemote = _portReceivingRemote;
            currentSequenceNumber = _currentSequenceNumber;
            tcpSession = _tcpClient;
            SessionStream = _clientSteam;
            p = _p;

            sessionKey = ipNumber + ";" + processID.ToString();

            timeOutTime = 20; // seconds
            keepAlive = true;

            SessionThread = new Thread(listenSession);
            SessionThread.Name = ipNumber + ";" + processID;
            SessionThread.Start();            
        }
    
        // DESTRUCTOR
        ~IpPhoneSession()
        {
            closePhoneSession();
        }

        // METHODS
        public bool closePhoneSession()
        {
            if (tcpSession != null)
            {
                tcpSession.Close();
                tcpSession = null;
            }
            if (SessionThread != null)
            {
                keepAlive = false;
            }
            if (syncTimer != null)
            {
                syncTimer.Stop();
                syncTimer = null;
            }
            return keepAlive;
        }
        
        private void listenSession()
        {
            bool keepClient;
            byte[] message = new byte[4096];
            int bytesRead;
            string incomingData;

            keepClient = true;

            if (p.dicIpPhoneSessions.Count <= 1)
            { // start Avaya connection
                p.startAvayaConnection();
            }

            syncTimer = new System.Timers.Timer();
            syncTimer.Interval = 30000; // 2MIN
            syncTimer.Elapsed += syncTimer_Elapsed;
            syncTimer.Start();

            while (keepClient)
            {
                bytesRead = 0;

                try
                {
                    //blocks until a client sends a message
                    bytesRead = SessionStream.Read(message, 0, 4096);
                }
                catch (Exception exc)
                {
                    //a socket error has occured
                    dbLogger.Instance.addLog("IpPhoneSession - listenSession : " + exc.ToString());
                    break;
                }

                if (bytesRead == 0)
                {
                    //the client has disconnected from the server
                    break;
                }

                //message has successfully been received
                ASCIIEncoding encoder = new ASCIIEncoding();
                incomingData = encoder.GetString(message, 0, bytesRead);

                //building clean JSON string
                incomingData = incomingData.Replace("/n", string.Empty);

                //interpret incoming data
                keepClient = sessionReceived(incomingData);
            }
        }

        private bool sessionReceived(string inc)
        {
            char[] sep = { ';' };
            RootObject JSON = null;
            Dictionary<string, string> JSONDECODE = null;

            try
            {
                JSON = JsonConvert.DeserializeObject<RootObject>(inc);
                JSONDECODE = deCode(JSON);
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("IpPhoneSession - sessionReceived : " + exc.ToString());
            }


            bool keepAlive = true;

            try
            {
                switch (JSONDECODE["TOS_type"])
                {
                    case "ACK":
                        break;
                    case "FIN":
                        //p = dicPhone[JSONDECODE["sID_phonenumber"]];
                        keepAlive = p.removeSession(JSONDECODE["sID_ipnumber"], JSONDECODE["sID_processid"]);
                        if (p.sessionCount() <= 0) { p.server.delPhoneFromDic(JSONDECODE["sID_phonenumber"]); }
                        break;
                    case "SYN":
                        resetSync(Convert.ToInt16(JSONDECODE["data"]));
                        break;
                    case "CMD":
                        switch (JSONDECODE["TOS_cmd"])
                        {
                            case "register":
                                //NOT ALLOWED
                                break;
                            case "makecall":
                                p.makeCall(JSONDECODE["data"]);
                                break;
                            case "setauxwork":
                                bool bln = false;
                                if (JSONDECODE["data"] == "true") { bln = true; }
                                else if (JSONDECODE["data"] == "false") { bln = false; }
                                p.setAux(bln);
                                break;
                            case "transfer":
                                string[] transferData = null;
                                transferData = JSONDECODE["data"].Split(sep[0]);
                                p.transferTo(transferData[0], Convert.ToBoolean(transferData[1]));
                                break;
                            case "endcall":
                                p.endActiveCall();
                                break;
                            case "subscribeevent":
                                string[] subscribeEventData = null;
                                subscribeEventData = JSONDECODE["data"].Split(sep[0]);
                                p.subscribeEvent(subscribeEventData[0], Convert.ToBoolean(subscribeEventData[1]));
                                break;
                        }
                        break;
                    case "EVT":
                        break;
                    case "RPT":
                        break;
                    case "ADM":
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("IpPhoneServer - checkReceived : " + exc.ToString());
            }
            return keepAlive;
        }

        private Dictionary<string, string> deCode(RootObject inc)
        {
            Dictionary<string, string> output = new Dictionary<string, string>();
            string command = "";
            string[] sOut = null;
            char[] sep = { ':' };

            try
            {
                command = inc.dsJSON.ttJSON[0].TOS;
                sOut = command.Split(sep);
            }
            catch (Exception exc)
            { dbLogger.Instance.addLog("ipPhoneServer - deCode : " + exc.ToString()); }

            try
            {
                output["signature"] = inc.dsJSON.ttJSON[0].signature;
                output["sID_phonenumber"] = inc.dsJSON.ttJSON[0].sourceID[0].phoneNumber;
                output["sID_ipnumber"] = inc.dsJSON.ttJSON[0].sourceID[0].ipNumber;
                output["sID_processid"] = inc.dsJSON.ttJSON[0].sourceID[0].processID.ToString();
                output["sID_portreceiving"] = inc.dsJSON.ttJSON[0].sourceID[0].portReceiving.ToString();
                output["sID_portsending"] = inc.dsJSON.ttJSON[0].sourceID[0].portSending.ToString();
                output["dID_phonenumber"] = inc.dsJSON.ttJSON[0].destinationID[0].phoneNumber;
                output["dID_ipnumber"] = inc.dsJSON.ttJSON[0].destinationID[0].ipNumber;
                output["dID_processid"] = inc.dsJSON.ttJSON[0].destinationID[0].processID.ToString();
                output["dID_portreceiving"] = inc.dsJSON.ttJSON[0].destinationID[0].portReceiving.ToString();
                output["dID_portsending"] = inc.dsJSON.ttJSON[0].destinationID[0].portSending.ToString();
                output["seqID"] = inc.dsJSON.ttJSON[0].sequenceID.ToString();
                output["TOS_type"] = sOut[0];
                output["TOS_cmd"] = sOut[1];
                output["data"] = inc.dsJSON.ttJSON[0].data.ToString();
            }
            catch (Exception exc)
            { dbLogger.Instance.addLog("ipPhoneServer - deCode : " + exc.ToString()); }

            return output;
        }

        public void resetSync(int value)
        {
            try
            {
                iSync = value;
            }
            catch (Exception exc) { /*LOG*/ }
        }

        private void syncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (iSync < 3)
            {
                iSync++;
                p.sendMessage("SYN", "", sessionKey, this);
            }
            else
            {
                //drop session
                p.sendMessage("FIN", "", "", this);
                if (p.sessionCount() <= 0) { p.server.delPhoneFromDic(p.extension); }
            }            
        }

    }
}
