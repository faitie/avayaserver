﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace server
{
    class dbLogger
    {
        Dictionary<string, string> dic;
        SQLiteDatabase db;

        private static volatile dbLogger instance;
        private static object syncRoot = new Object();

        // CONSTRUCTOR
        public static dbLogger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new dbLogger();
                    }
                }
                return instance;
            }
        }

        private dbLogger()
        {
            db = new SQLiteDatabase(); 
            dic = new Dictionary<string,string>();
        }

        // DESTRUCTOR
        ~dbLogger()
        {
            if (dic != null) { dic.Clear(); dic = null; }
            if (db != null) { db = null; }
        }

        public void addLog(string logString)
        {
            try
            {
                dic.Clear();
                dic.Add("extension", "0000001");
                dic.Add("ts", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                dic.Add("log", logString);

                writeLog(dic);
            }
            catch
            {
                MessageBox.Show("Error on add log");
            }
        }

        public void addLog(IpPhone phone, string logstring)
        {
            try
            {
                dic.Clear();
                dic.Add("extension", phone.extension);
                dic.Add("ts", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                dic.Add("log", logstring);

                writeLog(dic);
            }
            catch
            {
                MessageBox.Show("error on addLog");
            }
        }

        public void addLog(IpPhoneServer server, string logString)
        {
            try
            {
                dic.Clear();
                dic.Add("extension", "000000");
                dic.Add("ts", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                dic.Add("log", logString);

                writeLog(dic);
            }
            catch
            {
                MessageBox.Show("Error on add log");
            }
        }

        public DataTable getQuery(string q)
        {
            try
            {
                return db.GetDataTable(q);
            }
            catch(Exception exc)
            {
                MessageBox.Show("error on getQuery" + exc.ToString());
                return null;
            }
        }

        private void writeLog(Dictionary<string, string> dicLog)
        {
            try
            {
                db.Insert("dtLogging", dic);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in writing to Logger db : " + exc.ToString());
            }
        }

    }
}
