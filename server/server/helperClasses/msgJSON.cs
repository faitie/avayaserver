﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace server
{
    public class SourceID
    {
        public string phoneNumber { get; set; }
        public string ipNumber { get; set; }
        public int processID { get; set; }
        public int portReceiving { get; set; }
        public int portSending { get; set; }
    }

    public class DestinationID
    {
        public string phoneNumber { get; set; }
        public string ipNumber { get; set; }
        public int processID { get; set; }
        public int portReceiving { get; set; }
        public int portSending { get; set; }
    }

    public class TtJSON
    {
        public string signature { get; set; }
        public List<SourceID> sourceID { get; set; }
        public List<DestinationID> destinationID { get; set; }
        public int sequenceID { get; set; }
        public string TOS { get; set; }
        public string data { get; set; }
    }

    public class DsJSON
    {
        public List<TtJSON> ttJSON { get; set; }
    }

    public class RootObject
    {
        public DsJSON dsJSON { get; set; }
    }
}
