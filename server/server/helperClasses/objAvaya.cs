﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
//using System.Xml;
using Avaya.ApplicationEnablement.DMCC;

namespace server
{
    class objAvaya
    {
        // PUBLIC PROPERTIES
        public string cError = "";

        // WAITHANDLES
        private EventWaitHandle _WaitLineResponse       = new AutoResetEvent(false);
        private EventWaitHandle _WaitThirdPartyDevId    = new AutoResetEvent(false);
        private EventWaitHandle _WaitOutboundCall       = new AutoResetEvent(false);
        private EventWaitHandle _WaitRegister           = new AutoResetEvent(false);

        // PRIVATE PROPERTIES
        private ServiceProvider serviceProvider = null;
        private Device Device = null;
        private Phone Phone = null;
        private Phone.PhoneEvents phoneEvents = null;
        private ThirdPartyCallController ThirdParty = null;
        private ThirdPartyCallController.ThirdPartyCallControlEvents ThirdPartyCallEvents = null;

        public string       sServerIp { get; private set; }
        private int         iServerPort;
        private string      sAppName;
        private string      sUserName;
        private string      sPassword;
        private int         iSessionCleanupDelay;
        private int         iSessionDuration;
        private string      sProtocolVersion;
        private bool        bSecure;
        private Object      userState;
        private bool        bStartAutoKeepAlive;
        private bool        bAllowCertificationHostnameMismatch;
        private int         invokeId;
        private string      sExtensionPass;
        private string      sSwitchName;
        private string      sSwitchIpInterface;
        private bool        bControllableByOtherSessions;
        private string      sDeviceId;
        private string      phoneMonitor = "";

        private string      sMyExtention = "";
        private string      sCallTo = "";
        private string      sMyThirdPartyDevId = "";
        private int         iCurrentLine = 0;

        private string      sInboundThirdPartyDevId = "";
        private string      sInboundCallId = "";
        private string      sOutboundThirdPartyDevId = "";
        private string      sOutboundCallId = "";

        private int         iCommand; // 1 = Transfer,
        private string      sLampCom = "";
        
        private bool        blnRegisteredTerminal;
        private bool        blnDeviceId;
        private bool        blnAppSession;
        private int         terminalRegBusy;
        private int         resourceBusy;

        private bool        blnLampReturn;
        private string      sTPDIReturn;

        // PUBLIC EVENTS
        public event error pushError;
        public delegate void error(string str);

        public delegate void message(string str);
        public event message pushMessage;

        public delegate void onGetLampMode(bool bln);
        public event onGetLampMode pushLampMode;

        public delegate void onTerminalRegistered(bool bln);
        public event onTerminalRegistered pushTerminalRegistered;

        public delegate void incommingCall(string sIncommingCallId);
        public event incommingCall onIncommingCall;

        // CONSTRUCTOR
        public objAvaya(string myExtention)
        {
            sMyExtention = myExtention;
            serviceProvider = null;
            // Create a new ServiceProvider object
            serviceProvider = new ServiceProvider();

            // REGISTERING EVENT HANDLERS
            setServiceProviderCallBackEvents();

            // TO DO: fill in the following variables:
            sServerIp = "AES.ACCENT.BE";
            iServerPort = 4721; // default unsecure port
            sAppName = "appSessionFrom";
            sUserName = "tsapi";
            sPassword = "Tsapi01!";
            iSessionCleanupDelay = 60; // Seconds
            iSessionDuration = 180; // Seconds
            sSwitchName = "CM5";
            sSwitchIpInterface = "";
            sExtensionPass = "2580";
            bControllableByOtherSessions = true;
            // NOTE: The only protocol the .NET API officially supports is 4.1. However, you can try 3.0, 3.1, or 4.0.     
            sProtocolVersion = ServiceProvider.DmccProtocolVersion.PROTOCOL_VERSION_4_2;
            bSecure = false; // If set to true then IPPortToServer should be 4722 (default secure port)
            bStartAutoKeepAlive = true; // if this is set to false then you will have to call ResetApplicationSession every SessionDuration seconds
            // AllowCertificateNameMismatch is only necessary if you are using secure sockets. Setting AllowCertificateNameMismatch to true
            // will allow a Certificate Name Mismatch error to occur.
            bAllowCertificationHostnameMismatch = true;
        }

        // DESTRUCTOR
        ~objAvaya()
        {
            this.stopSession();
            serviceProvider = null;
            Device = null;
            Phone = null;
            phoneEvents = null;
        }

        // PUBLIC METHODS
        public void startSession()
        {
            try
            { 
                if (!blnAppSession && !blnDeviceId && !blnRegisteredTerminal)
                {
                    try
                    {
                        // InvokeId is the XML message number that was sent to the server
                        int InvokeId;

                        InvokeId = serviceProvider.StartApplicationSession(sServerIp, iServerPort, sAppName,
                                    sUserName, sPassword, iSessionCleanupDelay, iSessionDuration, sProtocolVersion,
                                    bSecure, serviceProvider, bStartAutoKeepAlive, bAllowCertificationHostnameMismatch);

                        Device = serviceProvider.GetNewDevice();
                        Phone = Device.getPhone;
                        ThirdParty = serviceProvider.getThirdPartyCallController;

                        setCallBackEvents();

                        setMonitors();

                        startMonitors();
                        
                    }
                    catch (Exception exc)
                    {
                        cError = cError + "Exception while trying to start the application session. If you are trying to use secure sockets then please make sure the Avaya certificate (avaya.crt) in installed on this PC. If you don’t have the certificate handy you might try setting the “Aes Socket Port” field to 4721 and unchecking “Secure Socket”. In order to use unsecure sockets, the AES must be configured to allow non secure socket communications. If it is not, please contact the person responsible for configuring the AES. Exception details: " + exc.Message;
                        //System.Console.WriteLine(cError);
                        pushError(cError);
                    }
                }
            }
            catch (Exception exc)
            {
                cError = cError + "Exception while trying to set startup parameters" + exc.Message;
                //System.Console.WriteLine(cError);
                pushError(cError);
            }
        }

        public void startSession( string ServerIP )
        {
            try
            { 
                if (!blnAppSession && !blnDeviceId && !blnRegisteredTerminal)
                {
                    try
                    {
                        sServerIp = ServerIP;
                        // InvokeId is the XML message number that was sent to the server
                        int InvokeId;

                        InvokeId = serviceProvider.StartApplicationSession(sServerIp, iServerPort, sAppName,
                                    sUserName, sPassword, iSessionCleanupDelay, iSessionDuration, sProtocolVersion,
                                    bSecure, serviceProvider, bStartAutoKeepAlive, bAllowCertificationHostnameMismatch);

                        Device = serviceProvider.GetNewDevice();
                        Phone = Device.getPhone;

                        setCallBackEvents();

                        // set phone events
                        phoneEvents.LampModeEvent = true;
                        phoneEvents.HookswitchEvent = true;
                        phoneEvents.TerminalUnregisteredEvent = true;
                        phoneEvents.RingerStatusEvent = true;

                        Device.getPhone.StartMonitor(phoneEvents, null);
                        
                    }
                    catch (Exception exc)
                    {
                        cError = cError + "Exception while trying to start the application session. If you are trying to use secure sockets then please make sure the Avaya certificate (avaya.crt) in installed on this PC. If you don’t have the certificate handy you might try setting the “Aes Socket Port” field to 4721 and unchecking “Secure Socket”. In order to use unsecure sockets, the AES must be configured to allow non secure socket communications. If it is not, please contact the person responsible for configuring the AES. Exception details: " + exc.Message;
                        //System.Console.WriteLine(cError);
                        pushError(cError);
                    }
                }
            }
            catch (Exception exc)
            {
                cError = cError + "Exception while trying to set startup parameters" + exc.Message;
                //System.Console.WriteLine(cError);
                pushError(cError);
            }
        }

        public void stopSession()
        {
            unRegisterTerminal();
        }

        public void makeCall(string telNr)
        {
            this.sCallTo = telNr;
            try
            {
                if (this.checkLine(this.iCurrentLine))
                {
                    this.setButton(this.iCurrentLine);
                    this.dailNr(this.sCallTo);
                }
                else
                {
                    if (this.iCurrentLine < 3)
                    {
                        this.holdActiveCall(this.iCurrentLine);
                        this.iCurrentLine++;
                        this.makeCall(this.sCallTo);
                    }
                    else { /* no more line free */ }
                }
            }
            catch (Exception exc)
            {

            }
        }

        public void endCall()
        {
            try
            {
                // invokeId is the XML message number that was sent to the server
                int InvokeId;
                // Assume currentDevice is the device that you want to go onhook or offhook on.
                InvokeId = Phone.SetHookswitchStatus(true, null);
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("objAvaya - endCall : " + exc.ToString());
            }


            //try
            //{
            //    int InvokeId;
            //    InvokeId = serviceProvider.getThirdPartyCallController.ClearCall(new ThirdPartyCallController.CallIdentifier(sMyThirdPartyDevId, sMyCallId), null);
            //}
            //catch (Exception exc)
            //{

            //}

        }

        public bool checkLine(int iLine)
        {
            this.checkLamp(iLine, "line");
            _WaitLineResponse.WaitOne();

            return blnLampReturn;
        }

        public bool checkAuxWork()
        {
            this.checkLamp(19, "AUX");
            return true;
        }

        public void setButton(int iButton)
        {
            iButton -= 1;
            Phone.ButtonIDConstants btn = new Phone.ButtonIDConstants();
            btn = Phone.ButtonIDConstants.BUTTON_1;
            btn += iButton;

            try
            {
                Phone.PressButton(btn, null);
            }
            catch (Exception exc)
            {
                pushError("setButton : " + exc.ToString());
            }
        }

        public void transferCall(string callTo,bool direct)
        {
            if (direct)
            {
                try
                {
                    string destinationDeviceId = getThirdPartyDeviceId(callTo);
                    int InvokeId;
                    InvokeId = serviceProvider.getThirdPartyCallController.SingleStepTransferCall(new ThirdPartyCallController.CallIdentifier(sInboundThirdPartyDevId, sInboundCallId), destinationDeviceId, null);

                }
                catch (Exception exc)
                {
                    dbLogger.Instance.addLog("objAvaya - transferCall : " + exc.ToString());
                }
            }
            else
            {
                try
                {
                    //this.makeCall(callTo);
                    // InvokeId is the XML Message number that was sent to the DMCC server.
                    int InvokeId;
                    InvokeId = serviceProvider.getThirdPartyCallController.TransferCall(new ThirdPartyCallController.CallIdentifier(sInboundThirdPartyDevId, sInboundCallId),
                                    new ThirdPartyCallController.CallIdentifier(sOutboundThirdPartyDevId, sOutboundCallId),
                                    null);

                }
                catch (Exception exc)
                {
                    dbLogger.Instance.addLog("objAvaya - transferCall : " + exc.ToString());
                }
            }
        }

        public void holdActiveCall(int iLine)
        {
            Phone.ButtonIDConstants btn = new Phone.ButtonIDConstants();
            btn = Phone.ButtonIDConstants.HOLD;

            try
            {
                this.setButton(iCurrentLine); // select current line
                Phone.PressButton(btn, null); // press hold for current line.
            }
            catch (Exception exc)
            {
                pushError("setButton : " + exc.ToString());
            }
        }
        
        public void subscribeEvent(string strEvent, bool blnEvent)
        {
            switch (strEvent)
            {
                case("inboundCalls"):
                    if (blnEvent) { ThirdParty.OnDeliveredEvent += new DeliveredEventHandler(ThirdParty_OnDeliveredEvent); }
                    else {ThirdParty.OnDeliveredEvent -= new DeliveredEventHandler(ThirdParty_OnDeliveredEvent); }
                    ThirdPartyCallEvents.DeliveredEvent = blnEvent;
                    break;
                case("outboundCalls"):
                    if (blnEvent) { ThirdParty.OnOriginatedEvent += new OriginatedEventHandler(ThirdParty_OnOriginatedEvent); }
                    else { ThirdParty.OnOriginatedEvent -= new OriginatedEventHandler(ThirdParty_OnOriginatedEvent); }
                    ThirdPartyCallEvents.OriginatedEvent = blnEvent;
                    break;
                case ("transferCallsSingleStep"):
                    if (blnEvent) { ThirdParty.OnSingleStepTransferCallResponse += new SingleStepTransferCallResponseHandler(ThirdParty_SingleStepTransferEvent); }
                    else { ThirdParty.OnSingleStepTransferCallResponse -= new SingleStepTransferCallResponseHandler(ThirdParty_SingleStepTransferEvent); }
                    ThirdPartyCallEvents.TransferredEvent = blnEvent;
                    break;
                case("transferCallsThreeStep"):
                    if (blnEvent) { ThirdParty.OnTransferCallResponse += new TransferCallResponseHandler(ThirdParty_OnTransferCallResponse); }
                    else { ThirdParty.OnTransferCallResponse -= new TransferCallResponseHandler(ThirdParty_OnTransferCallResponse); }
                    ThirdPartyCallEvents.TransferredEvent = blnEvent;
                    break;
            }
        }

        // PRIVATE METHODS
        private void setCallBackEvents()
        {
            // Device
            Device.OnGetDeviceIdResponse += new GetDeviceIdResponseHandler(Device_OnGetDeviceIdResponse);
            Device.OnReleaseDeviceIdResponse += new ReleaseDeviceIdResponseHandler(Device_OnReleaseDeviceIdResponse);

            // Phone
            Phone.OnHookswitchUpdatedEvent += new HookswitchUpdatedEventHandler(Phone_OnHookswitchUpdatedEvent);
            Phone.OnRegisterTerminalResponse += new RegisterTerminalResponseHandler(Phone_OnRegisterTerminalResponse);
            Phone.OnUnregisterTerminalResponse += new UnregisterTerminalResponseHandler(Phone_OnUnregisterTerminalResponse);
            Phone.OnGetRegistrationStateResponse += new GetRegistrationStateResponseHandler(Phone_OnGetRegistrationStateResponse);
            Phone.OnGetLampModeResponse += new GetLampModeResponseHandler(Phone_OnGetLampModeResponse);
            Phone.OnStopMonitorResponse += new PhoneStopMonitorResponseHandler(Phone_OnStopMonitorResponse);
            Phone.OnStartMonitorResponse += new PhoneStartMonitorResponseHandler(Phone_OnStartMonitorResponse);

            // ThirdParyDev
            ThirdParty.OnGetThirdPartyDeviceIdResponse += new GetThirdPartyDeviceIdResponseHandler(ThirdParty_OnGetThirdPartyDeviceIdResponse);
            ThirdParty.OnConnectionClearedEvent += new ConnectionClearedEventHandler(ThirdParty_OnConnectionClearedResponse);
        }

        private void setServiceProviderCallBackEvents()
        {
            serviceProvider.OnStartApplicationSessionResponse += new StartApplicationSessionResponseHandler(serviceProvider_OnStartApplicationSessionResponse);
            serviceProvider.OnStopApplicationSessionResponse += new StopApplicationSessionResponseHandler(serviceProvider_OnStopApplicationSessionResponse);
        }

        private void registerTerminal()
        {
            try
            {
                // NOTE: You should have registered for the OnRegisterTerminal repsonse so 
                // you know when the terminal was registered and if there were any errors.
                // There are code snippets to show you how to do this.
                
                // Get and create the objects necesary for the RegisterTerminal method
                Avaya.ApplicationEnablement.DMCC.Phone.LoginInfo LoginInfo;
                Avaya.ApplicationEnablement.DMCC.Phone.MediaInfo MediaInfo;

                LoginInfo = new Avaya.ApplicationEnablement.DMCC.Phone.LoginInfo();
                
                MediaInfo = new Avaya.ApplicationEnablement.DMCC.Phone.MediaInfo();
                // TO DO: Set the extension's password and force login attributes.
                // NOTE: As of this writing, the server ONLY support true for ForceLogin
                LoginInfo.ExtensionPassword = "2580";
                LoginInfo.ForceLogin = true;

                // TO DO: If you are registering for Client Media mode then
                // you must specify the codec. We will default it to G.711U, change it
                // if necessary. If you are registering in any other media mode
                // then you can just leave this as is.
                // The current valid values for the codec are:
                //    "g711U"
                //    "g711A"
                //    "g729"
                //    "g729A"
                //    "g723"    
                MediaInfo.CodecList = new List<string>();
                MediaInfo.CodecList.Add("g711U");
                
                // TO DO: Add encryption schemes. 
                // We will assume no media encryption will be done.
                MediaInfo.EncryptionList = new List<string>();
                MediaInfo.EncryptionList.Add("none");

                // TO DO: Need to set the appropriate Media Dependency Mode
                // NOTE: Dependency modes are only used if your DMCC server is running version 4.0 or later.
                MediaInfo.RequestedDependencyMode = Phone.MediaInfo.DependencyMode.Independent;                       
                //MediaInfo.RequestedDependencyMode = Phone.MediaInfo.DependencyMode.Dependent;                       
                //MediaInfo.RequestedDependencyMode = Phone.MediaInfo.DependencyMode.Main;

                // TO DO: Select the media mode you want to register the application in:
                // We will configure it for no media. If you want another mode then
                // just uncomment it and comment out NONE.
                // NOTE: A MediaMode of NONE is what used to be called Shared Control
                MediaInfo.MediaControl = Media.MediaMode.NONE;    
                // MediaInfo.MediaControl = Media.MediaMode.TELECOMMUTER;           
                // MediaInfo.MediaControl = Media.MediaMode.SERVER_MODE;           
                //MediaInfo.MediaControl = Media.MediaMode.CLIENT_MODE;      
                        
                // If you are registering in Telecommter mode then you must specify a number.
                // If not, then just set leave this set to ""
                MediaInfo.TelecommuteNumber = "";
                
                // TO DO: Now you must specify the destination for the RTP and RTCP packets.
                // This is only necessary if you are registering in CLIENT_MODE. If you are
                // registering in other modes, just leave them set to ""   
                MediaInfo.RtpIpAddress = "";
                MediaInfo.RtpIpPort = "";
                MediaInfo.RtcpIpAddress = "";
                MediaInfo.RtcpIpPort = "";
                
                // InvokeId is the XML message number that was sent to the server
                int InvokeId;
                InvokeId = Device.getPhone.RegisterTerminal(LoginInfo, MediaInfo, sMyExtention);
                //t.Start();
            }
            catch (Exception exc)
            {
                dbLogger.Instance.addLog("objAvaya - registerTerminal : " + exc.ToString());
            }
        }

        private void getRegistrationState()
        {
            Phone.GetRegistrationState(sDeviceId);
        }

        private void unRegisterTerminal()
        {
            if (blnAppSession && blnDeviceId && blnRegisteredTerminal)
            {
                try
                {
                    // InvokeId is the XML message number that was sent to the server
                    int InvokeId;
                    // TO DO: Change the arguments as appropriate
                    InvokeId = Device.getPhone.UnregisterTerminal(null);
                }
                catch (Exception exc)
                {
                    // TO DO: Put in your exception code.
                    pushError("Exception while attepting UnregisterTerminal: " + exc.Message);
                }
            }
            else if (blnAppSession && blnDeviceId && !blnRegisteredTerminal)
            {
                blnRegisteredTerminal = false;
                try
                {
                    phoneEvents.LampModeEvent = false;
                    phoneEvents.TerminalUnregisteredEvent = false;
                    phoneEvents.RingerStatusEvent = false;
                    phoneEvents.HookswitchEvent = false;
                    Phone.StopMonitor(phoneMonitor, null);
                }
                catch (Exception exc)
                {
                    pushError("Exception while attempting to stop monitor: " + exc.Message);
                }
            }
            else if (blnAppSession && !blnDeviceId && !blnRegisteredTerminal)
            {
                try
                {
                    // InvokeId is the XML message number that was sent to the server
                    int InvokeId;
                    // TO DO: Change the arguments as appropriate
                    InvokeId = serviceProvider.StopApplicationSession("Application Request", null);
                }
                catch (Exception exc)
                {
                    // TO DO: Put in your exception code.
                    pushError("Exception while attepting StopApplicationSession: " + exc.Message);
                }
            }
        }

        private void setMonitors()
        {
            // set phone events
            phoneEvents = new Phone.PhoneEvents(false);
            phoneEvents.LampModeEvent = true;
            phoneEvents.HookswitchEvent = false;
            phoneEvents.TerminalUnregisteredEvent = true;
            phoneEvents.RingerStatusEvent = false;

            // set ThirdPartyCall events
            ThirdPartyCallEvents = new ThirdPartyCallController.ThirdPartyCallControlEvents(false);
            ThirdPartyCallEvents.EstablishedEvent = true;
            ThirdPartyCallEvents.ConnectionClearedEvent = true;
        }

        private void startMonitors()
        {
            try
            {
                Phone.StartMonitor(phoneEvents, userState);
                ThirdParty.StartMonitor(sMyThirdPartyDevId, ThirdPartyCallEvents, userState);
            }
            catch (Exception exc)
            {
                // write to log.
            }
        }

        private void getThirdPartyDeviceId()
        {
            if (sMyExtention.Length != 0 && serviceProvider != null && blnRegisteredTerminal == true)
            {
                ThirdParty.GetThirdPartyDeviceId(sSwitchName, sMyExtention, null);
            }
        }

        private string getThirdPartyDeviceId(string sExtension)
        {
            if (sMyExtention.Length != 0 && serviceProvider != null && blnRegisteredTerminal == true)
            {
                ThirdParty.GetThirdPartyDeviceId(sSwitchName, sExtension, null);
                _WaitThirdPartyDevId.WaitOne();
            }
            else { sTPDIReturn = ""; }

            return sTPDIReturn;
        }

        private void dailNr(string telNr)
        {
            if (blnRegisteredTerminal)
            {
                //Phone.PressButton(Phone.ButtonIDConstants.DIGIT_0, null);
                for (int i = 0; i < telNr.Length; i++)
                {
                    string digit = telNr.Substring(i, 1);
                    switch (digit)
                    {
                        case "0":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_0, null);
                            break;
                        case "1":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_1, null);
                            break;
                        case "2":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_2, null);
                            break;
                        case "3":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_3, null);
                            break;
                        case "4":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_4, null);
                            break;
                        case "5":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_5, null);
                            break;
                        case "6":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_6, null);
                            break;
                        case "7":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_7, null);
                            break;
                        case "8":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_8, null);
                            break;
                        case "9":
                            Phone.PressButton(Phone.ButtonIDConstants.DIGIT_9, null);
                            break;
                    }
                }
                Phone.PressButton(Phone.ButtonIDConstants.DIGIT_POUND, null);

                _WaitOutboundCall.WaitOne();
            }
        }

        private void checkLamp(int iLamp, string command)
        {
            this.sLampCom = command;
            iLamp -= 1;
            Phone.ButtonIDConstants btn = new Phone.ButtonIDConstants();
            btn = Phone.ButtonIDConstants.BUTTON_1;
            btn += iLamp;

            Phone.GetLampMode(btn, null);
        }

        // EVENT HANDLERS
        private void serviceProvider_OnStartApplicationSessionResponse(object sender, ServiceProvider.StartApplicationSessionResponseArgs e)
        {
            if (e.getServiceProvider.getSessionId.Length != 0)
            {
                blnAppSession = true;
                if (blnAppSession && !blnDeviceId && !blnRegisteredTerminal)
                {
                    try
                    {
                        //pushMessage("applicationSessionResponse OK - " + sMyExtention);
                        Device.GetDeviceId(sMyExtention, sSwitchName, sSwitchIpInterface, bControllableByOtherSessions, userState);
                    }
                    catch (Exception exc)
                    {
                        pushMessage("applicationSessionResponse NIET OK - " + sMyExtention);
                    }
                }
            }
            else
            {
                blnAppSession = false;
            }
        }

        private void Device_OnGetDeviceIdResponse(object sender, Device.GetDeviceIdResponseArgs e)
        {
            if (e.getDevice.getDeviceIdAsString.Length != 0)
            {
                blnDeviceId = true;
                if (blnAppSession && blnDeviceId && !blnRegisteredTerminal)
                {
                    try
                    {
                        sDeviceId = e.getDevice.getDeviceIdAsString.ToString();
                        //pushMessage("deviceId OK - " + sMyExtention);
                        registerTerminal();
                    }
                    catch (Exception exc)
                    {
                        pushMessage("deviceId NIET OK - " + sMyExtention);
                    }
                }
            }
            else
            {
                blnDeviceId = false;
                if (e.getError.IndexOf("<systemResourceAvailibility>resourceBusy</systemResourceAvailibility>") > 0)
                {
                    this.stopSession();
                    if (resourceBusy < 5)
                    {
                        resourceBusy++;
                        this.startSession();
                    }
                    else
                    {
                        pushError("onGetDeviceIdResponse - resourceBusy - Gelieve IT te contacteren en deze melding mee te delen.");
                    }
                }
                
            }
        }

        private void Phone_OnRegisterTerminalResponse(object sender, Phone.RegisterTerminalResponseArgs e)
        {
            if (e.getPhone.getRegistered)
            {
                blnRegisteredTerminal = true;
                getThirdPartyDeviceId();
                this.iCurrentLine = 1;
                pushMessage("TERMINAL REGISTERED - " + sMyExtention);
            }
            else
            {
                terminalRegBusy++;
                blnRegisteredTerminal = false;
                if (terminalRegBusy == 1) { this.startSession("172.22.100.20"); }
                else if (terminalRegBusy == 2) { this.startSession("172.22.100.21"); }
                //else if (terminalRegBusy > 2) { pushMessage("TERMINAL COULD NOT REGISTER WITH EXTENSION : " + sMyExtention); }
                else if (terminalRegBusy > 2) { getRegistrationState(); }

                pushMessage("TERMINAL NIET REGISTERED - " + sMyExtention); 
            }
            pushTerminalRegistered(blnRegisteredTerminal);
        }

        private void Phone_OnHookswitchUpdatedEvent(object sender, Phone.HookswitchUpdatedEventArgs e)
        {
            pushMessage("HOOKSWITCH - " + sMyExtention);
        }

        private void Phone_OnUnregisterTerminalResponse(object sender, Phone.UnregisterTerminalResponseArgs e)
        {
            blnRegisteredTerminal = false;
            try
            {
                phoneEvents.LampModeEvent = false;
                phoneEvents.TerminalUnregisteredEvent = false;
                phoneEvents.RingerStatusEvent = false;
                phoneEvents.HookswitchEvent = false;
                Phone.StopMonitor(phoneMonitor, null);
            }
            catch (Exception exc)
            {
                pushError("Exception while attempting to stop monitor: " + exc.Message);
            }
        }

        private void Phone_OnStartMonitorResponse(object sender, Phone.StartMonitorResponseArgs e)
        {
            phoneMonitor = e.getMonitorId;
        }

        private void Phone_OnStopMonitorResponse(object sender, Phone.PhoneStopMonitorResponseArgs e)
        {
            if (!blnRegisteredTerminal && blnDeviceId && blnAppSession)
            {
                try
                {
                    Device.ReleaseDeviceId(null);
                }
                catch (Exception exc)
                {
                    pushError("Exception while attempting to Release deviceID: " + exc.Message);
                }
            }
        }

        private void Device_OnReleaseDeviceIdResponse(object sender, Device.ReleaseDeviceIdResponseArgs e)
        {
            blnDeviceId = false;
            if (!blnRegisteredTerminal && !blnDeviceId && blnAppSession)
            {
                try
                {
                    // InvokeId is the XML message number that was sent to the server
                    int InvokeId;
                    // TO DO: Change the arguments as appropriate
                    InvokeId = serviceProvider.StopApplicationSession("Application Request", null);
                }
                catch (Exception exc)
                {
                    // TO DO: Put in your exception code.
                    pushError("Exception while attepting StopApplicationSession: " + exc.Message);
                }
            }
        }

        private void serviceProvider_OnStopApplicationSessionResponse(object sender, ServiceProvider.StopApplicationSessionResponseArgs e)
        {
            blnAppSession = false;
            pushMessage("ApplicationSession Stoped : " + sMyExtention);
        }
        
        private void Phone_OnGetRegistrationStateResponse(object sender, Phone.GetRegistrationStateResponseArgs e)
        {
            string sState = e.getRegistrationState.ToString();
            if (sState != "REGISTERED")
            {
                try
                {
                    // InvokeId is the XML message number that was sent to the server
                    int InvokeId;

                    // The sysStatRegisterID for the registration that is being cancelled.
                    string sysStatRegisterID = "?";

                    InvokeId = serviceProvider.SystemRegisterCancel(sysStatRegisterID, null);

                }
                catch (Exception exc)
                {
                    // TO DO: Add exception handling code here.
                    pushError(sender.ToString() + " Exception: " + exc.Message);
                }
            }
        }

        private void Phone_OnGetLampModeResponse(object sender, Phone.GetLampModeResponseArgs e)
        {
            string lampMode = "";

            if (e.getLampModeList != null)
            {
                if (this.sLampCom == "AUX")
                {
                    lampMode = e.getLampModeList[0].getLampMode.ToString();
                    if (lampMode.ToLower() == "off") { pushLampMode(false); }
                    else { pushLampMode(true); }
                }
                else if (this.sLampCom == "line")
                {
                    lampMode = e.getLampModeList[0].getLampMode.ToString();
                    if (lampMode.ToLower() == "off") 
                    {
                        this.blnLampReturn = true;
                        //this.getThirdPartyDeviceId(this.sCallTo, 2);
                    }
                    else 
                    {
                        this.blnLampReturn = false;
                        //this.iCurrentLine++; 
                        //this.checkLamp(this.iCurrentLine,"line"); 
                    }
                    _WaitLineResponse.Set();
                }
            }
        }

        private void ThirdParty_OnGetThirdPartyDeviceIdResponse(object sender, ThirdPartyCallController.GetThirdPartyDeviceIdResponseArgs e)
        {
            if (e.getError.IndexOf("invalidParameterValue") > 0)
            {
                // write error to log
            }
            else
            {
                if (sMyThirdPartyDevId.Length == 0)
                {
                    sMyThirdPartyDevId = e.getDeviceIdAsString;
                    startMonitors();
                }
                else
                {
                    sTPDIReturn = e.getDeviceIdAsString;
                    _WaitThirdPartyDevId.Set();
                }
            }
        }

        private void ThirdParty_OnDeliveredEvent(object sender, ThirdPartyCallController.DeliveredEventEventArgs e)
        {
            string nr = e.getCallingDeviceId.Substring(0,e.getCallingDeviceId.IndexOf(':'));
            if (nr != sMyExtention)
            {
                pushMessage("INBOUND CALL : " + e.getCallingDeviceId);

                sInboundCallId = e.getConnectionId.getCallId;
                sInboundThirdPartyDevId = e.getCalledDeviceId;

                onIncommingCall(e.getCallingDeviceId);
            }
        }

        private void ThirdParty_OnOriginatedEvent(object sender, ThirdPartyCallController.OriginatedEventArgs e)
        {
            pushMessage("OUTBOUND CALL : " + e.getCalledDeviceId);

            sOutboundCallId = e.getOriginatedConnectionId.getCallId;
            sOutboundThirdPartyDevId = e.getCallingDeviceId;

            _WaitOutboundCall.Set();
        }

        private void ThirdParty_SingleStepTransferEvent(object sender, ThirdPartyCallController.SingleStepTransferCallResponseArgs e)
        {
            pushMessage("CALL TRANSFER : " + e.getTransfereeDeviceId); 
        }

        private void ThirdParty_OnTransferCallResponse(object sender, ThirdPartyCallController.TransferCallResponseArgs e)
        {
            pushMessage("CALL TRANSFER : " + e.getTransferredCallConnectionId.getDeviceIdAsString); 
        }

        private void ThirdParty_OnConnectionClearedResponse(object sender, ThirdPartyCallController.ConnectionClearedEventArgs e)
        {
            this.iCurrentLine = 1;
        }

    }
}